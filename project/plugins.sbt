addSbtPlugin("io.spray" % "sbt-revolver" % "0.7.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "0.8.0")
addSbtPlugin("org.ensime" % "ensime-sbt" % "0.1.7")
