--create extension btree_gist;
--create extension fuzzystrmatch;

--
-- Format timestamps according to ISO 8601
--
--create function to_iso_time(t timestamp with time zone) returns varchar as $$
--  begin
--    return to_char(t at time zone 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS.MS"Z"');
--  end;
--$$ language plpgsql
--   immutable
--   returns null on null input;

--
-- User
--
create sequence user_id;

create table users(
	id bigint primary key default nextval('user_id'),
	name varchar,
    firstname varchar,
    email varchar not null unique,
    password varchar not null,
    address varchar,

    -- Algorithm configuration
    timezone varchar not null,
    daystart time not null,
    dayend time not null
);

create unique index user_email_index on users(email);

--
-- Location / Routes
--
create sequence location_id;

create table locations(
    id bigint primary key default nextval('location_id'),
    owner bigint not null references users(id) on delete cascade,
    geo_name varchar not null,
    users_name varchar,
    longitude double precision not null,
    latitude double precision not null
);

create table routes(
    startid bigint not null references locations(id) on delete cascade,
    endid bigint not null references locations(id) on delete cascade,
    duration int not null, -- min
    distance int not null, -- m

    primary key(startid, endid)
);

--
-- Todo
--
create sequence todo_id;

create table todos(
	id bigint primary key default nextval('todo_id'),
    owner bigint not null references users(id) on delete cascade,
    name varchar not null,
    location bigint references locations(id),
    description varchar,
    duration interval not null,
    time_fence tstzrange,
    tags json,

    planned_time tstzrange not null,
    execution_start timestamptz,
    execution_end timestamptz
);

create index todo_time_index on todos using GiST(owner, planned_time);

-- Appointment interval factory method
--   Instances are generated from a starttime and a period to the next appointment
--   If the endtime is unspecified the number of appointments is infinit
create table appointment_interval_factory(
    id bigserial primary key,
    starttime timestamptz,
    endtime timestamptz,
    period interval,
    duration interval
);

create sequence appointment_factory_id;

create table appointment_factories(
    id bigint primary key default nextval('appointment_factory_id'),
    owner bigint not null references users(id) on delete cascade,
    name varchar not null,
    location bigint references locations(id),
    description varchar,
    tags json,
    interval_factory bigint references appointment_interval_factory(id)
);

-- Appointment list factory method:
--   Instances are generated from a finit list of dates
create table appointment_list_factory(
    id bigint not null references appointment_factories(id) on delete cascade,
    time tstzrange not null,
    primary key(id, time)
);

create table appointment_exceptions(
    id bigint not null references appointment_factories(id) on delete cascade,
    old_time timestamptz not null,
    time tstzrange,
    location bigint references locations(id),
    primary key (id, old_time)
);

-- create index appointment_time_index on appointments using GiST(owner, time);

-- Hangout
create sequence hangout_id;

--create type timerange as range (subtype = time);

create table hangouts(
    id bigint primary key default nextval('hangout_id'),
    owner bigint not null references users(id) on delete cascade,
    name varchar not null,
    location bigint references locations(id),
    description varchar,
    duration interval not null, -- in min
    schedule_period tstzrange,
		schedule_day_time_start time,
		schedule_day_time_end time,
    tags json not null,
    time tstzrange not null
);

create table attendees(
    hangout_id bigint not null references hangouts(id) on delete cascade,
    user_id bigint not null references users(id) on delete cascade,
		accepted boolean,
    primary key (hangout_id, user_id)
);

-- Appointment groups
create sequence institution_id;
create table institutions(
	id bigint primary key default nextval('institution_id'),
	university varchar not null,
	faculty varchar not null
);

create sequence appointment_group_id;
create table appointment_groups(
	id bigint primary key default nextval('appointment_group_id'),
	institution bigint not null references institutions(id) on delete cascade,
	name varchar not null
);

create table appointment_group_items(
	id bigint references appointment_groups(id) on delete cascade,
	appointment_id bigint references appointment_factories(id) on delete cascade,
	primary key (id, appointment_id)
);
create index appointment_group_item_id_index on appointment_group_items(id);

create table users_appointment_groups(
	user_id bigint references users(id) on delete cascade,
	appointment_group_id bigint references appointment_groups(id) on delete cascade,
	primary key(user_id, appointment_group_id)
);
create index users_appointment_groups_user_id_index on users_appointment_groups(user_id);
