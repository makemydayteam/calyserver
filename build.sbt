import NativePackagerKeys._

name := "caly"

version := "1.0"

scalaVersion := "2.11.6"

scalacOptions := Seq("-Xlint", "-unchecked", "-deprecation", "-encoding", "utf8", "-feature")

libraryDependencies ++= {
	val akkaV = "2.3.9"
	val sprayV = "1.3.3"
	Seq(

		/*
		 * Akka
		 */
	  "com.typesafe.akka"   %%  "akka-actor"    % akkaV,

		/*
		 * Spray
		 */
	  "io.spray"            %%  "spray-can"     % sprayV,
    "io.spray"            %%  "spray-routing" % sprayV,
    "io.spray"            %%  "spray-json"    % "1.3.1",
    "io.spray"            %%  "spray-client"  % sprayV,

		/*
		 * Slick
		 */
    "com.typesafe.slick"  %%  "slick"         % "3.0.1",
		"com.typesafe.slick"  %% "slick-codegen"  % "3.0.1",
		"org.postgresql"      %   "postgresql"    % "9.4-1201-jdbc41",
		"com.github.tminglei" %%  "slick-pg"      % "0.9.2",
		"com.zaxxer"          %  "HikariCP-java6" % "2.3.5",

		/*
		 * Utils
		 */
		"org.slf4j"           %  "slf4j-nop"      % "1.6.4",
		"joda-time"           %  "joda-time"      % "2.7",
		"org.joda"            %  "joda-convert"   % "1.7",

		/*
		 * Test
		 */
		"org.scalatest" 			%% "scalatest"      % "2.2.4" % "test",
		"org.scalamock"       %% "scalamock-scalatest-support" % "3.2" % "test"
	)
}

Revolver.settings

packageArchetype.java_application
