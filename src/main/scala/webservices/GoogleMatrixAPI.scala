package com.example.webservices

import com.example.model.Coordinates

import org.joda.time

import scala.concurrent.Future

import akka.actor.ActorSystem
import akka.io.IO

import spray.can.Http
import spray.util._

import spray.http._
import spray.httpx.SprayJsonSupport._
import spray.client.pipelining._

import spray.json._
import DefaultJsonProtocol._
import com.example.util.BasicJsonProtocol

object GoogleMatrix extends Google with DefaultJsonProtocol {

	implicit val system = ActorSystem("spray-client")
	import system.dispatcher

	private val matrixBaseUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?"

	private def buildSeperatorString(coords: Seq[Coordinates]) = (coords.map(((coord: Coordinates) => s"${coord.latitude},${coord.longitude}"))).mkString("|")

	private def buildMatrixUrl(origins: Seq[Coordinates], destinations: Seq[Coordinates], mode: String, language: String) : String = {

		val _key = "key=" + key
		val _origins = "&origins=" + buildSeperatorString(origins)
		val _destinations = "&destinations=" + buildSeperatorString(destinations)
		val _mode = "&mode=" + mode
		val _language = "&language=" + language

		matrixBaseUrl + _key + _origins + _destinations + _mode + _language
	}


	case class Distance(text: String, value: Int)
	case class Duration(text: String, value: Int)
	case class Element(distance: Distance, duration: Duration, status: String)
	case class Row(elements: Seq[Element])
	case class GoogleMatrixResponse(destination_addresses: Seq[String], origin_addresses: Seq[String], rows: Seq[Row], status: String)

	implicit val distanceFormat = jsonFormat2(Distance)
	implicit val durationFormat = jsonFormat2(Duration)
	implicit val elementFormat = jsonFormat3(Element)
	implicit val rowFormat = jsonFormat1(Row)
	implicit val googleMatrixResponseFormat = jsonFormat4(GoogleMatrixResponse)


	def getDistanceVector(origin: Coordinates, destinations: Seq[Coordinates]) : Future[Seq[(Int, time.Duration)]] = {

		val url = buildMatrixUrl(Seq[Coordinates](origin), destinations, "driving", "de")

		val _pipeline: HttpRequest => Future[GoogleMatrixResponse] = sendReceive ~> unmarshal[GoogleMatrixResponse]

		_pipeline(Get(url)).map(_.rows.head.elements.map(e => (e.distance.value, new time.Duration((e.duration.value.toLong)*1000))))
	}

}
