package com.example.webservices

import com.example.model.Coordinates

import org.joda.time.DateTime

import scala.concurrent.Future

import akka.actor.ActorSystem
import akka.io.IO

import spray.can.Http
import spray.util._

import spray.http._
import spray.httpx.SprayJsonSupport._
import spray.client.pipelining._

import spray.json._
import DefaultJsonProtocol._
import com.example.util.BasicJsonProtocol


object HereMapsMatrix extends BasicJsonProtocol with HereMaps {

	implicit val system = ActorSystem("spray-client")
	import system.dispatcher

	private val matrixBaseUrl = "http://route.st.nlp.nokia.com/routing/6.2/calculatematrix.json"
	private val mode = "&mode=fastest;car;traffic:disabled"

	private def buildMatrixUrl(start: Coordinates, destination: Seq[Coordinates]) : String = {
		val _start = s"&start0=${start.latitude},${start.longitude}"

		val _destination : (String, Int) = destination.foldLeft(("", 0)) { (state, coord) =>
			(state._1 + s"&destination${state._2}=${coord.latitude},${coord.longitude}", state._2+1)
		}

		matrixBaseUrl + credentials + _start + _destination._1 + mode
	}


	case class KeyValue(key: String, value: String)
	case class MetaInfo(Timestamp: DateTime, AdditionalData: Seq[KeyValue])

	case class RouteSummary(Distance: Double, BaseTime: Double)
	case class Route(Summary: RouteSummary)
	case class MatrixEntry(StartIndex: Int, DestinationIndex: Int, Route: Route)

	case class ResponseData(MetaInfo: MetaInfo, MatrixEntry: Seq[MatrixEntry])
	case class Response(Response: ResponseData)

	/*
	 * Here maps
	 */
	implicit val hereKeyValueFormat = jsonFormat2(KeyValue)
	implicit val hereMetaInfoFormat = jsonFormat2(MetaInfo)

	implicit val hereRouteSummaryFormat = jsonFormat2(RouteSummary)
	implicit val hereRouteFormat = jsonFormat1(Route)
	implicit val hereMatrixEntryFormat = jsonFormat3(MatrixEntry)

	implicit val hereResponseDataFormat = jsonFormat2(ResponseData)
	implicit val hereResponseFormat = jsonFormat1(Response)


	def getDistanceMatrix(start: Coordinates, destination: Seq[Coordinates]) : Future[Seq[RouteSummary]] = {
		val url = buildMatrixUrl(start, destination)

		println("URL: " + url)

		val pipeline: HttpRequest => Future[Response] = sendReceive ~> unmarshal[Response]

		pipeline(Get(url)).map(r => r.Response.MatrixEntry.map(m => m.Route.Summary))
	}

}
