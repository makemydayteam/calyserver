package com.example.webservices

import com.example.model.Hangout

import org.joda.time.DateTime

import scala.concurrent.Future

import akka.actor.ActorSystem
import akka.io.IO

import spray.can.Http
import spray.util._

import spray.http._
import spray.httpx.SprayJsonSupport._
import spray.client.pipelining._

import spray.json._
import DefaultJsonProtocol._
import com.example.util.BasicJsonProtocol

object ParsePushService extends DefaultJsonProtocol with BasicJsonProtocol {
  implicit val system = ActorSystem("push_service")
	import system.dispatcher

  private val applicationId = "f9T7flvVEP8tSuRjqZUcFHaQBcqnYDB3f4vji7Lp"
  private val apiKey = "QS91S2LxqZ5AE4jCDAKYJndNldQY4ZxfV7UrrPUy"

  case class Query(user_id: Long)
  case class Data(alert: String, date: DateTime)
  case class PushNotification(where: Query, data: Data)

  case class Result(result: Boolean)

  implicit val queryFormat = jsonFormat1(Query)
  implicit val dataFormat = jsonFormat2(Data)
  implicit val pushNotificationFormat = jsonFormat2(PushNotification)
  implicit val resultFormat = jsonFormat1(Result)

  def notifyHangoutChange(userId: Long, date: DateTime) : Future[Boolean] = {
    val data = PushNotification(Query(userId), Data("Hangout suggestion", date))

    val pipeline: HttpRequest => Future[Result] = (
      addHeader("X-Parse-Application-Id", applicationId)
      ~> addHeader("X-Parse-REST-API-Key", apiKey)
      ~> sendReceive
      ~> unmarshal[Result]
    )

    val response: Future[Result] =
      pipeline(Post("https://api.parse.com/1/push", data))

    response.map(_.result)
  }
}
