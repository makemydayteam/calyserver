package com.example.algorithm

import com.example.model.db.AppointmentFactoryDB

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait AppointmentFactoryController {
	val db : AppointmentFactoryDB

	def addAppointmentFactory = db.addAppointmentFactory(_)

	def getAppointmentFactory = db.getAppointmentFactory(_)

	def updateAppointmentFactory = db.updateAppointmentFactory(_)

	def deleteAppointmentFactory = db.deleteAppointmentFactory(_)
}