package com.example.algorithm

import com.example.model.db.{TodoDB, DayPlanDB}

import com.example.model.{NewTodo, PlannedTodo, Todo, DayPlanItem, TodoItem, AppointmentItem, User}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import org.joda.time.{DateTime, Minutes, LocalTime, Duration, Interval}

trait TodoController extends PlanningAlgorithm {
	val db : TodoDB with DayPlanDB

	def addTodo(user: User, todo: NewTodo): Future[Option[Todo]] = {
		planTodo2(
			user,
			todo.duration,
			todo.timeFence.getOrElse(new Interval(new DateTime, new DateTime().plusDays(6)))
		).flatMap {
			case interval +: _  =>
				println(s"Got ${interval}")
				val plannedTodo = PlannedTodo(interval, todo)

				db.addTodo(plannedTodo).flatMap { t =>
					updateDayPlan2(user.id, interval.getStart.withTime(0,0,0,0), interval.getEnd.withTime(23,59,59,999)).flatMap { changes =>
						Future.sequence(changes.map( t => db.updateTodo(t))).map(x => t);
					}
				}

			case _ =>
				println("no match!!!")
				Future(None)
		}
	}

	def haveToReplan(oldTodo: Todo, newTodo: Todo) : Boolean = {
		val durationTooLong = newTodo.duration.isLongerThan(oldTodo.plannedTime.toDuration)

		lazy val notInSchedulingWindow = newTodo.timeFence match {
			case Some(i: Interval) =>
				i.contains(oldTodo.plannedTime)

			case _ => false
		}

		durationTooLong || notInSchedulingWindow
	}

	def updateTodo(user: User, todo: Todo) : Future[Option[Todo]] = {
		db.getTodo(todo.id).flatMap {
			case Some(oldTodo) =>
				(todo.executionStart,todo.executionEnd) match {

					case (Some(s), Some(e)) =>
						val doneTodo = todo.copy(plannedTime = new Interval(s, e), duration=new Duration(s,e))

						db.updateTodo(doneTodo).flatMap { t =>
							updateDayPlan2(user.id, oldTodo.plannedTime.getStart.withTime(0,0,0,0), oldTodo.plannedTime.getEnd.withTime(23,59,59,999)).flatMap { changes =>
								Future.sequence(changes.map( t => db.updateTodo(t))).map(_ => t);
							}
						}

					case (Some(s), None) =>
						val startedTodo = todo.copy(plannedTime = new Interval(s, oldTodo.plannedTime.getEnd))

						db.updateTodo(startedTodo).flatMap { t =>
							updateDayPlan2(user.id, oldTodo.plannedTime.getStart.withTime(0,0,0,0), oldTodo.plannedTime.getEnd.withTime(23,59,59,999)).flatMap { changes =>
								Future.sequence(changes.map( t => db.updateTodo(t))).map(_ => t);
							}
						}

					case (None, None) => 
						if (haveToReplan(oldTodo, todo)) {
							println("haveToReplan")
							planTodo2(user, todo.duration, todo.timeFence.getOrElse(new Interval(new DateTime, new DateTime().plusDays(6)))).flatMap {
								case interval +: _  =>
									println(s"got interval ${interval}")
									val plannedTodo = todo.copy(plannedTime = interval)

									db.updateTodo(plannedTodo).flatMap { t =>
										updateDayPlan2(user.id, interval.getStart.withTime(0,0,0,0), interval.getEnd.withTime(23,59,59,999)).flatMap { changes =>
											Future.sequence(changes.map( t => db.updateTodo(t))).map(_ => t);
										}
									}

								case _ =>
									println("no interval")
									Future(None)
							}
						} else {
							println("no replan")
							val plannedTodo = todo.copy(plannedTime = oldTodo.plannedTime)

							db.updateTodo(plannedTodo).flatMap { t =>
										updateDayPlan2(user.id, oldTodo.plannedTime.getStart.withTime(0,0,0,0), oldTodo.plannedTime.getEnd.withTime(23,59,59,999)).flatMap { changes =>
											Future.sequence(changes.map( t => db.updateTodo(t))).map(_ => t);
										}
									}
						}

					case (None, Some(e)) =>
						println("executionEnd without executionStart")
						Future(None)

				}

			case _ =>
				println("get faild")
				Future(None)
		}
	}

	def postponeTodo(user: User, id: Long) : Future[Option[Todo]] = {
		db.getTodo(id).flatMap {
			case Some(todo) =>

				try {

					val timeFence = {
						val start = getOrCurrTime(todo.plannedTime.getEnd)

						todo.timeFence match {
							case Some(i) => i.withStart(start)
							case _ => new Interval(start, start.plusDays(6))
						}
					}

					planTodo2(user, todo.duration, timeFence).flatMap {
							case interval +: _  =>
								println(s"Postpone: ${interval}")

								val plannedTodo = todo.copy(plannedTime = interval)

								db.updateTodo(plannedTodo).flatMap { t =>
									updateDayPlan2(user.id, todo.plannedTime.getStart.withTime(0,0,0,0), todo.plannedTime.getEnd.withTime(23,59,59,999)).flatMap { changesOldPosition =>
										updateDayPlan2(user.id, interval.getStart.withTime(0,0,0,0), interval.getEnd.withTime(23,59,59,999)).flatMap { changes =>
											Future.sequence((changesOldPosition ++ changes).map( t => db.updateTodo(t))).map(_ => t);
										}
									}
								}

							case _ =>
								println("Postpone: no interval :-(")
								Future(None)
					}

				} catch {
					case e: Exception =>
						println(s"Postpone: ${e}")
						Future(None)
				}

			case _ => Future(None)
		}
	}

	case class ChangedItem(changed: Boolean, item: DayPlanItem)

	def realignTodoStart2(plan: Seq[DayPlanItem]) : Seq[ChangedItem] = {
		case class State(pre: Option[DayPlanItem] = None, changes: Seq[ChangedItem] = Seq())

		val state = plan . foldLeft (State()) {
			case (State(Some(pre), changes), item) =>
				(pre, item) match {
					case (AppointmentItem(a), TodoItem(t)) if(t.executionStart.isDefined == false) =>
						t.withPlannedTime(getOrCurrTime(a.time.getEnd), t.plannedTime.getEnd) match {
							case Some(t_) =>
								val newItem = item.copy(todo=Some(t_))
								State(Some(newItem), ChangedItem(true, newItem) +: changes)
							case _ =>
								State(Some(item), ChangedItem(false, item) +: changes)
						}

					case (TodoItem(t1), TodoItem(t2)) if(t2.executionStart.isDefined == false) =>
						val start = getOrCurrTime { (t1.executionStart, t1.executionEnd) match {
							case (Some(s), Some(e)) => e
							case (Some(s), None) => s.plus(t1.duration)
							case _ => t1.plannedTime.getStart.plus(t1.duration)
						}}

						t2.withPlannedTime(start, t2.plannedTime.getEnd) match {
							case Some(t_) =>
								val newItem = item.copy(todo=Some(t_))
								State(Some(newItem), ChangedItem(true, newItem) +: changes)
							
							case _ =>
								State(Some(item), ChangedItem(false, item) +: changes)
						}

					case _ => State(Some(item), ChangedItem(false, item) +: changes)
				}

			case (State(None, changes), item) => State(Some(item), ChangedItem(false, item) +: changes)
		}

		state.changes
	}

	def realignTodoEnd2(plan: Seq[ChangedItem]) : Seq[ChangedItem] = {
		case class State(post: Option[ChangedItem] = None, changes: Seq[ChangedItem] = Seq())

		val state = plan . foldLeft (State()) {
			case (State(Some(post), changes), item) =>
				(item, post) match {
					case (ChangedItem(c, TodoItem(t)), ChangedItem(_, AppointmentItem(a))) =>
						t.withPlannedTime(t.plannedTime.getStart, a.time.getStart) match {
							case Some(t_) =>
								val newItem = ChangedItem(true, DayPlanItem(todo=Some(t_), starttime=t_.plannedTime.getStart, endtime=t_.plannedTime.getEnd))
								State(Some(newItem), newItem +: changes)
							
							case _ => State(Some(item), item +: changes)
						}
					case (ChangedItem(c, TodoItem(t1)), ChangedItem(_, TodoItem(t2))) =>
						t1.withPlannedTime(t1.plannedTime.getStart, t2.latestExecTime.getStart) match {
							case Some(t_) =>
								val newItem = ChangedItem(true, DayPlanItem(todo=Some(t_), starttime=t_.plannedTime.getStart, endtime=t_.plannedTime.getEnd))
								State(Some(newItem), newItem +: changes)
							
							case _ => State(Some(item), item +: changes)
						}
					case _ => State(Some(item), item +: changes)
				}

			case (State(None, changes), item) => State(Some(item), item +: changes)
		}

		state.changes
	}

	def updateDayPlan2(user: Long, start: DateTime, end: DateTime) : Future[Seq[Todo]] = {
		val userDayStart = new LocalTime(8, 0)
		val userDayEnd = new LocalTime(20, 0)
		val day_sentinals = getDaySentinals(start, end, userDayStart, userDayEnd)
		val plan = db.getDayPlan(user, start.withTime(0,0,0,0), end)
		val plan_with_sentinals = plan.map{p => (p ++ day_sentinals) sortBy (_.endtime.getMillis)}

		plan_with_sentinals map { p =>
			realignTodoEnd2(realignTodoStart2(p)).filter(_.changed).map(_.item.todo.get)
		}
	}

	def getTodo(id: Long) = db.getTodo(id)

	def deleteTodo(id: Long) = {
		db.getTodo(id).flatMap {
			case Some(todo) =>
				val start = todo.plannedTime.getStart.withTime(0,0,0,0)
				val end = todo.plannedTime.getEnd.withTime(23,59,59,999)

				db.deleteTodo(id).flatMap { success =>
					updateDayPlan2(todo.owner, start, end).flatMap { changes =>
						Future.sequence(changes.map( t => db.updateTodo(t))).map(_ => success);
					}
				}

			case _ => Future(false)
		}
	}

	def replanOverdueTodos(user: User) : Future[Boolean] = {
		db.getOverdueTodos(user.id, new DateTime().withTime(0,0,0,0)).flatMap { todos =>
			Future.sequence{
				todos.map { t =>
					postponeTodo(user, t.id)
				}
			}.map(_ => true)
		}
	}

}
