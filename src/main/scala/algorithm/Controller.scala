package com.example.algorithm

import com.example.model.DBConnection
import com.example.model.db.{UserDB, LocationDB, TodoDB, DayPlanDB, HangoutDB, AppointmentFactoryDB, AppointmentFactoryGroupDB}

object Controller
	extends UserController
	with LocationController
	with TodoController
	with HangoutController
	with DayPlanController
	with AppointmentFactoryController {

	object DBAccess
		extends DBConnection
		with UserDB
		with LocationDB
		with TodoDB
		with DayPlanDB
		with HangoutDB
		with AppointmentFactoryDB
		with AppointmentFactoryGroupDB {

	}

	val db = DBAccess
}
