package com.example.algorithm

import com.example.model.db.LocationDB

import com.example.model.{Location, JsonLocation}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait LocationController {
	val db : LocationDB

	def addLocation(location: JsonLocation) : Future[Option[JsonLocation]] = {
		location.toLocation match {
			case Some(l) =>	db.addLocation(l) map {
				_ map {
					JsonLocation(_)
				}
			}

			case _ => Future(None)
		}
	}


	def updateLocation(location: JsonLocation) : Future[Option[JsonLocation]] = {
		location.toLocation match {
			case Some(l) =>	db.updateLocation(l) map {
				_ map {
					JsonLocation(_)
				}
			}
			case _ => Future[Option[JsonLocation]](None)
		}
	}

	def getLocation(id: Long) : Future[Option[JsonLocation]] = {
		db.getLocation(id) map {
			_ map {
				JsonLocation(_)
			}
		}
	}

	def getAllUserLocations(userid: Long) : Future[Seq[JsonLocation]] = {
		db.getAllUserLocations(userid) map {
			_ map {
				JsonLocation(_)
			}
		}
	}

	def searchForLocation(userid: Long, searchText: String) : Future[Seq[JsonLocation]] = {
		db.searchForLocation(userid, searchText) map {
			_ map {
				JsonLocation(_)
			}
		}
	}
}
