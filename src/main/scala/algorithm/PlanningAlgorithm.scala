package com.example.algorithm

import org.joda.time.{DateTime, DateTimeZone, Days, Duration, Interval, Period, LocalTime}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import scala.annotation.tailrec

import com.example.model._
import com.example.model.db.DayPlanDB

trait PlanningAlgorithm {

	val db : DayPlanDB

	def dateRange(from: DateTime, to: DateTime, step: Period): Iterator[DateTime] = Iterator.iterate(from)(_.plus(step)).takeWhile(!_.isAfter(to))

	def getDaySentinals(from: DateTime, to: DateTime, dayStart: LocalTime, dayEnd: LocalTime) : Iterator[DayPlanItem] = {
		dateRange(
			from.withTime(0,0,0,0),
			to.withTime(0,0,0,0).plusDays(1), // add one day to get a day end sential for the last day 
			Days.ONE.toPeriod
		).map { d =>
			
			val time = 
				if(dayEnd.isAfter(dayStart)) { // => you go to bed before 0:00
					val end   = d.withTime(dayEnd).minusDays(1)
					val start = d.withTime(dayStart)

					new Interval(end, start)
				} else {
					val end = d.withTime(dayEnd)
					val start = d.withTime(dayStart)

					new Interval(end, start)
				}

			DayPlanItem(
				appointment = Some(Appointment(
					id = -1,
					owner = -1,
					name = "sentinal",
					time = time,
					tags=Seq()
				)),
				starttime = time.getStart,
				endtime = time.getEnd
			)
		}
	}

	def getOrCurrTime(time: DateTime) = {
		val currTime = new DateTime

		if(time.isAfter(currTime))
			time
		else
			currTime
	}
/*
	def checkCoherence(oldPlan: Option[PlannedTime], item: DayPlanItem, todo: Todo) : Boolean = oldPlan match {
		case Some(p) =>
			val tags = if(item.todo.isDefined) item.todo.get.tags else item.appointment.get.tags
			val addTime = 0

			addTime < p.additionalTime || (!p.cluster && tags == todo.tags)

		case _ => true
	}
*/

	def checkPosition(time: Interval, schedulingInterval: Interval, duration: Duration) : Option[Interval] = {

		val earliestStart : DateTime = getOrCurrTime {
			val s = time.getStart

			if (s.isBefore(schedulingInterval.getStart))
				schedulingInterval.getStart
			else
				s
		}

		val latestEnd : DateTime = getOrCurrTime {
			val e = time.getEnd

			if(e.isAfter(schedulingInterval.getEnd))
				schedulingInterval.getEnd
			else
				e
		}

		if(earliestStart.isBefore(latestEnd)) {
			val plannedInterval = new Interval(earliestStart, latestEnd)

			if (plannedInterval.toDuration.isLongerThan(duration) || plannedInterval.toDuration.isEqual(duration))
				Some(plannedInterval)
			else
				None
		} else {
			None
		}
	}

	def planTodo2(user: User, duration: Duration, timeFence: Interval) : Future[Seq[Interval]] = {

		def withUsersTimezone(t: DateTime) : DateTime = {
			t.toDateTime(DateTimeZone.forID(user.timezone))
		}

		val schedulingInterval = new Interval(
			withUsersTimezone(timeFence.getStart),
			withUsersTimezone(timeFence.getEnd)
		)

		/*
		 * Get day plan and add sentinals
		 */
		val day_sentinals = getDaySentinals(
			schedulingInterval.getStart,
			schedulingInterval.getEnd,
			user.dayStart, user.dayEnd
		)
		
		val plan = db.getDayPlan(
			user.id,
			schedulingInterval.getStart.withTime(0,0,0,0),
			schedulingInterval.getEnd
		)
		
		val plan_with_sentinals = plan.map{p => (p ++ day_sentinals) sortBy (_.endtime.getMillis)}

		val plan_pairs = plan_with_sentinals map (p => p zip p.tail)

		plan_pairs . map { (p: Seq[(DayPlanItem, DayPlanItem)]) =>
			p . foldLeft (Seq[Interval]()) { (state: Seq[Interval], pair: (DayPlanItem, DayPlanItem)) =>
				val interval : Option[Interval] =
					pair match {
						case (FixedTimeItem(i1), FixedTimeItem(i2)) =>
							checkPosition(new Interval(i1.getEnd, i2.getStart), schedulingInterval, duration)
						case (FixedTimeItem(i1), TodoItem(t)) if(!t.inExecution) =>
							checkPosition(new Interval(i1.getEnd, t.latestExecTime.getStart), schedulingInterval, duration)
						case (TodoItem(t), FixedTimeItem(i2)) =>
							checkPosition(new Interval(t.earliestExecTime.getEnd, i2.getStart), schedulingInterval, duration)
						case (TodoItem(t1), TodoItem(t2)) if (!t2.inExecution) =>
							checkPosition(new Interval(t1.earliestExecTime.getEnd, t2.latestExecTime.getStart), schedulingInterval, duration)
						case _ => None
					}

				interval match {
					case Some(i) => state :+ i
					case _ => state
				}
			}
		}
	}

}
