package com.example.algorithm

import com.example.model.db.UserDB

import com.example.model.NewUser
import spray.routing.authentication.UserPass

trait UserController {
	val db : UserDB

	def addUser(user: NewUser) = db.addUser(user)

	def getUser(email: String) = db.getUser(email)
	def getUser(id: Long) = db.getUser(id)

	def updateUser(oldEmail: String, user: NewUser) = db.updateUser(oldEmail, user)

	def deleteUser(email: String) = db.deleteUser(email)

	def authenticate(userPass: Option[UserPass]) = db.authenticate(userPass)
}
