package com.example.algorithm

import com.example.model.db.DayPlanDB

import com.example.model.{Appointment, Todo, DayPlanItem, User}

import org.joda.time.{DateTime, LocalTime, Period, Days, Interval}


import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait DayPlanController extends TodoController {
	val db : DayPlanDB

	def getDayPlan(user: User, starttime: DateTime, endtime: DateTime) = replanOverdueTodos(user).flatMap { _ =>
		db.getDayPlan(user.id, starttime, endtime)
	}

}
