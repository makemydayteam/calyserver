package com.example.algorithm

import com.example.model.db.{HangoutDB, UserDB, DayPlanDB}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import com.example.model.{Hangout, User}

import org.joda.time.{DateTime, Interval, Days, Duration}

import com.example.webservices.ParsePushService

trait HangoutController extends PlanningAlgorithm {
	val db : HangoutDB with UserDB with DayPlanDB

	def planHangout(user: User, hangout: Hangout) = {
		val users = hangout.owner.get +: hangout.attendees.map(_.id)

		val duration = hangout.duration.toStandardDuration
		val schedulePeriod = hangout.timeFence.getOrElse(new Interval(new DateTime, new DateTime().plusDays(6)))

		val scheduleDayTime : Option[Seq[(DateTime, DateTime)]] = hangout.dayTime.map { t =>
			dateRange(
				schedulePeriod.getStart.withTime(0,0,0,0),
				schedulePeriod.getEnd.withTime(0,0,0,0).plusDays(1), // add one day to get a day end sential for the last day
				Days.ONE.toPeriod
			).map { d =>
					(d.withTime(t.start),d.withTime(t.end))
			}.toSeq
		}

		val intervals : Seq[Future[Seq[(DateTime, DateTime)]]] = users.map { id =>
			db.getUser(id)
		}.map { _.flatMap {
				case Some(u) =>
					planTodo2(u, duration, schedulePeriod).map(_.map{i => (i.getStart, i.getEnd)})

				case None =>
					Future(Seq())
			}
		}

		val time : Future[Seq[(DateTime, DateTime)]] = Future.sequence(intervals).map { _.fold(Seq[(DateTime, DateTime)]((schedulePeriod.getStart, schedulePeriod.getEnd))) { (i1, i2) =>
				combineIntervalLists(i1, i2).filter(_.isDefined).map(_.get)
			}
		}.map {
			seq =>
				scheduleDayTime.map { t =>
					combineIntervalLists(seq, t).filter(_.isDefined).map(_.get)
				}.getOrElse(seq)
		}.map {
			_.filter {
				case (s,e) =>
					val d = new Duration(s,e)
					d.equals(duration) || d.isLongerThan(duration)
			}
		}

		time
	}

	def addHangout(user: User, hangout: Hangout) : Future[Option[Hangout]] = {
		planHangout(user, hangout).flatMap { time =>
			time.headOption match {
				case Some(t) =>
					val r = db.addHangout(hangout.copy(time = Some(new Interval(t._1, t._1.plus(hangout.duration))) ))

					hangout.attendees foreach { a =>
						ParsePushService.notifyHangoutChange(a.id, t._1.withTimeAtStartOfDay)
					}

					r

				case _ => Future(None)
			}
		}
	}

	def getHangout(user: User, id: Long) : Future[Option[Hangout]] = {
		db.getHangout(id).flatMap {
			case Some(hangout) =>
				if (hangout.owner.get == user.id)
					Future(Some(hangout.copy(isOwner=Some(true))))
				else {
					db.getAcceptanceStateHangout(user.id, id).map { (state: Option[Boolean]) =>
						Some(hangout.copy(isOwner=Some(false), accepted=state))
					}
				}

			case _ => Future(None)
		}
	}

	def updateHangout(app: Hangout) = db.updateHangout(app)
	def deleteHangout(id: Long) = db.deleteHangout(id)

	def acceptHangout(user: User, id: Long) = db.acceptHangout(user.id, id)
	def rejectHangout(user: User, id: Long) = db.rejectHangout(user.id, id)

	def postponeHangout(user: User, hangoutId: Long) = {
		db.getHangout(hangoutId).flatMap {
			case Some(h) if (h.owner.get == user.id) =>
				planHangout(user, h).flatMap { times =>
					times.filter(_._1.isAfter(h.time.get.getStart)).headOption match {
						case Some(t) =>
							val newTime = new Interval(t._1, t._1.plus(h.duration))
							val clear = db.clearHangoutAcceptance(hangoutId)
							val upd = db.updateHangoutTime(hangoutId, newTime)

							val r = Future.sequence(Seq(clear, upd)).map {
								_ => Some(h.copy(time = Some(newTime)))
							}

							h.attendees foreach { a =>
								ParsePushService.notifyHangoutChange(a.id, t._1.withTimeAtStartOfDay)
							}

							r

						case None =>
							Future(Some(h))
						}
					}

			case _ =>
				Future(None)
		}
	}

	def intervalIntersection(int1: (DateTime, DateTime), int2: (DateTime, DateTime)) : Option[(DateTime, DateTime)] = {


		def intersect(int1: (DateTime, DateTime), int2: (DateTime, DateTime)) : Option[(DateTime, DateTime)] = {
			if(int1._2.isAfter(int2._1)) {
				// intersection
				Some((
					int2._1,
					if(int1._2.isBefore(int2._2))
						int1._2
					else
						int2._2
				))
			} else {
				None
			}
		}

		if (int1._1.isBefore(int2._1))
			intersect(int1, int2)
		else
			intersect(int2, int1)
	}

	def combineIntervalLists(int1: Seq[(DateTime, DateTime)], int2: Seq[(DateTime, DateTime)]) : Seq[Option[(DateTime, DateTime)]] = {
		if (int1.isEmpty || int2.isEmpty) {
			Seq()
		} else {

			val elem1 = int1.head
			val elem2 = int2.head

			val newInt1 =
				if(elem1._2.isAfter(elem2._2))
					int1
				else
					int1.tail

			val newInt2 =
				if(elem1._2.isAfter(elem2._2))
					int2.tail
				else
					int2

			intervalIntersection(elem1,elem2) +: combineIntervalLists(newInt1, newInt2)
		}
	}
}
