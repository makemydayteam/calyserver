package com.example

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import spray.can.Http
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._

import com.typesafe.config.ConfigFactory

import web.MyServiceActor

object Boot extends App {

	println("Start server ...")

  // we need an ActorSystem to host our application in
  implicit val system = ActorSystem("on-spray-can")

  // create and start our service actors
  //val appointmentService = system.actorOf(Props[AppointmentActor], "appointment-service")
  val service = system.actorOf(Props[MyServiceActor], "demo-service")

  implicit val timeout = Timeout(5.seconds)
  // start a new HTTP server on port 8080 with our service actor as the handler
  val conf = ConfigFactory.load();
  IO(Http) ? Http.Bind(service, interface = "0.0.0.0", port = conf.getInt("server.port"))
}
