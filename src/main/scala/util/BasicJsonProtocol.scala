package com.example.util

import org.joda.time.{DateTime, LocalTime, Interval, Period, Duration}
import org.joda.time.format.ISODateTimeFormat

import spray.json._
import DefaultJsonProtocol._

trait BasicJsonProtocol extends DefaultJsonProtocol {

	implicit object DateTimeJsonFormat extends JsonFormat[DateTime] {
		val format = ISODateTimeFormat.dateTime

		def write(t: DateTime) = JsString(format.print(t))

		def read(value: JsValue) = value match {
			case JsString(dateTimeString) => DateTime.parse(dateTimeString, ISODateTimeFormat.dateTimeParser)
			case _ => deserializationError("Iso date time expected")
		}
	}

	implicit object LocalTimeJsonFormat extends JsonFormat[LocalTime] {
		//val format = ISODateTimeFormat.dateTime

		def write(t: LocalTime) = JsString(t.toString)

		def read(value: JsValue) = value match {
			case JsString(dateTimeString) => LocalTime.parse(dateTimeString)
			case _ => deserializationError("Iso date time expected")
		}
	}

	implicit object IntervalJsonFormat extends JsonFormat[Interval] {
		def write(i: Interval) = JsString(i.toString)

		def read(value: JsValue) = value match {
			case JsString(intervalString) => Interval.parse(intervalString)
			case _ => deserializationError("Iso interval expected")
		}
	}

	implicit object PeriodJsonFormat extends JsonFormat[Period] {
		def write(i: Period) = JsString(i.toString)

		def read(value: JsValue) : Period = value match {
			case JsString(periodString) => Period.parse(periodString)
			case _ => deserializationError("Iso interval expected")
		}
	}

	implicit object DurationJsonFormat extends JsonFormat[Duration] {
		def write(i: Duration) = JsString(i.toString)

		def read(value: JsValue) : Duration = value match {
			case JsString(periodString) => Duration.parse(periodString)
			case _ => deserializationError("Iso interval expected")
		}
	}
}
