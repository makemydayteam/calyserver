package com.example.model

import org.joda.time.{DateTime, Interval, Duration}

sealed abstract class AbstractTodo {
	def name: String

	def duration: Duration

	def timeFence: Option[Interval]

	def executionStart: Option[DateTime]

	def executionEnd: Option[DateTime]

	def location: Option[AbstractLocation]

	def description: Option[String]

	def tags: Seq[String]

	def isValid = !name.trim.isEmpty
}

sealed trait IdentifiableTodo {
	def id : Long
}

sealed trait OwnableTodo {
	def owner : Long
}

sealed trait PlanableTodo {
	def plannedTime : Interval
	def duration: Duration

	def earliestExecTime : Interval = {
		val s = plannedTime.getStart
		new Interval(s, s.plus(duration))
	}

	def latestExecTime : Interval = {
		val e = plannedTime.getEnd
		new Interval(e.minus(duration), e)
	}
}

case class ClientTodo(
	name: String,
	duration: Duration,
	timeFence: Option[Interval],
	executionStart: Option[DateTime],
	executionEnd: Option[DateTime],
	location: Option[AbstractLocation],
	description: Option[String],
	tags: Seq[String]
) extends AbstractTodo

case class NewTodo(
	owner: Long,
	name: String,
	duration: Duration,
	timeFence: Option[Interval],
	executionStart: Option[DateTime],
	executionEnd: Option[DateTime],
	location: Option[AbstractLocation],
	description: Option[String],
	tags: Seq[String]
) extends AbstractTodo with OwnableTodo

object NewTodo {
	def apply(owner: Long, t: ClientTodo) : NewTodo = {
		NewTodo(owner, t.name, t.duration, t.timeFence, t.executionStart, t.executionEnd, t.location, t.description, t.tags)
	}
}

case class PlannedTodo(
	owner: Long,
	name: String,
	duration: Duration,
	timeFence: Option[Interval],
	executionStart: Option[DateTime],
	executionEnd: Option[DateTime],
	location: Option[AbstractLocation],
	description: Option[String],
	tags: Seq[String],
	plannedTime: Interval
) extends AbstractTodo with OwnableTodo with PlanableTodo

object PlannedTodo {
	def apply(plannedTime: Interval, t: NewTodo) : PlannedTodo = {
		PlannedTodo(t.owner, t.name, t.duration, t.timeFence, t.executionStart, t.executionEnd, t.location, t.description, t.tags, plannedTime)
	}
}

case class Todo(
	id: Long,
	owner: Long,
	name: String,
	duration: Duration,
	timeFence: Option[Interval],
	executionStart: Option[DateTime],
	executionEnd: Option[DateTime],
	location: Option[AbstractLocation],
	description: Option[String],
	tags: Seq[String],
	plannedTime: Interval
) extends AbstractTodo with IdentifiableTodo with OwnableTodo with PlanableTodo {
	def withPlannedTime(starttime: DateTime, endtime: DateTime) : Option[Todo] = {
		try {
			val time = new Interval(starttime, endtime)
			if ((!timeFence.isDefined || timeFence.contains(time)) && (time.toDuration.isLongerThan(duration) || time.toDuration.isEqual(duration))) {
				Some(this.copy(plannedTime = time))
			} else {
				None
			}
		} catch {
		  case e: Exception => None
		}
	}

	def inExecution : Boolean = executionStart.isDefined && !executionEnd.isDefined

	override def earliestExecTime : Interval = {
		if(done) {
			new Interval(executionStart.get, executionEnd.get)
		} else if(inExecution) {
			new Interval(executionStart.get, executionStart.get.plus(duration))
		} else {
			super.earliestExecTime
		}
	}

	def done : Boolean = executionStart.isDefined && executionEnd.isDefined
}

object Todo {
	def apply(id: Long, t: PlannedTodo) : Todo = {
		Todo(id, t.owner, t.name, t.duration, t.timeFence, t.executionStart, t.executionEnd, t.location, t.description, t.tags, t.plannedTime)
	}
}
