package com.example.model

case class AppointmentGroupsRow (
  id: Long,
  institution: Long,
  name: String
)

case class AppointmentGroup (
  id: Long,
  institution: Long,
  name: String,
  subscribed: Boolean
)
