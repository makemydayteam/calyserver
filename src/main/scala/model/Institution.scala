package com.example.model

case class InstitutionRow (
  id: Long,
  university: String,
  faculty: String
)
