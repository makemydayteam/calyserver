package com.example.model.db

import com.github.tminglei.slickpg.`[_,_]`
import com.github.tminglei.slickpg.Range
import com.example.model.MyPostgresDriver.api._
import slick.model.ForeignKeyAction
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import spray.json._
import DefaultJsonProtocol._

import com.example.model.{Todo, Appointment, Location, DayPlanItem, AppointmentFactory, Hangout}

import org.joda.time.{DateTime, Interval}

trait DayPlanDB extends TodoDB with AppointmentFactoryDB with AppointmentFactoryGroupDB with HangoutDB {

	val db : Database

	/*
	 * DayPlan
	 */

	def _getDayTodos(userid: Long, starttime: DateTime, endtime: DateTime) : Future[Seq[DayPlanItem]] = {
		var j = for {
			(t,l) <- todos.filter(t => t.owner === userid && t.plannedTime @& Range(starttime,endtime).bind) joinLeft locations on (_.location === _.id)
		} yield (t, l)

		var query = j.result

		db.run(query).map(_ map {j => {
			var t : TodosRow = j._1
			var l: Option[Location] = j._2

			val todo = Todo(
				t.id,
				t.owner,
				t.name,
				t.duration.toStandardDuration,
				t.timeFence.map(t => new Interval(t.start, t.end)),
				t.executionStart,
				t.executionEnd,
				l,
				t.description,
				t.tags.convertTo[Seq[String]],
				new Interval(t.plannedTime.start, t.plannedTime.end))

			val (starttime, endtime) = (todo.executionStart, todo.executionEnd) match {
				case (Some(s), Some(e)) => (s, e)
				case (Some(s), None) => (s, s.plus(todo.duration))
				case _ => (todo.plannedTime.getStart, todo.plannedTime.getEnd)
			}

			DayPlanItem(
				todo = Some(todo),
				starttime = starttime,
				endtime = endtime)
		}})
	}

	def _getDayHangouts(userid: Long, starttime: DateTime, endtime: DateTime) : Future[Seq[DayPlanItem]] = {
		val hangouts : Future[Seq[Hangout]] = getHangout(userid, new Interval(starttime, endtime))

		hangouts map {
			for {
				h <- _
			} yield DayPlanItem(hangout = Some(h), starttime = h.time.get.getStart, endtime=h.time.get.getEnd)
		}
	}

	def _getDayAppointments(userid: Long, starttime: DateTime, endtime: DateTime) : Future[Seq[DayPlanItem]] = {
		val fac = getAppointmentFactoryByOwner(userid)

		val instances : Future[Seq[Appointment]] = fac.map {
			_ . foldLeft (Seq[Appointment]()) { (a : Seq[Appointment], b : AppointmentFactory) =>
				a ++ b.getInstances(new Interval(starttime, endtime))
			}
		}

		instances.map {
			_ map { app =>
				DayPlanItem(appointment = Some(app), starttime = app.time.getStart, endtime=app.time.getEnd)
			}
		}

	}

	def _getAppointmentGroups(userid: Long, starttime: DateTime, endtime: DateTime) : Future[Seq[DayPlanItem]] = {
		val fac = getAppointmentGroupFactoriesByUser(userid)

		val instances : Future[Seq[Appointment]] = fac.map {
			_ . foldLeft (Seq[Appointment]()) { (a : Seq[Appointment], b : AppointmentFactory) =>
				a ++ b.getInstances(new Interval(starttime, endtime))
			}
		}

		instances.map {
			_ map { app =>
				DayPlanItem(appointment = Some(app), starttime = app.time.getStart, endtime=app.time.getEnd)
			}
		}
	}

	def getDayPlan(userid: Long, starttime: DateTime, endtime: DateTime) : Future[Seq[DayPlanItem]] = {
		val appointments = _getDayAppointments(userid, starttime, endtime)
		val appointmentGroups = _getAppointmentGroups(userid, starttime, endtime)
		val todos = _getDayTodos(userid, starttime, endtime)
		val hangouts = _getDayHangouts(userid, starttime, endtime)

		for {
			a <- appointments
			g <- appointmentGroups
			t <- todos
			h <- hangouts
		} yield {
			(a ++ g ++ t ++ h) sortBy (_.starttime.getMillis)
		}

	}
}
