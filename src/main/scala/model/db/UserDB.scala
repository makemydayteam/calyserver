package com.example.model.db

import com.github.tminglei.slickpg.`[_,_]`
import com.example.model.MyPostgresDriver.api._
import slick.model.ForeignKeyAction
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import com.example.model.{User, NewUser, AbstractUser}
import spray.routing.authentication.UserPass
import org.joda.time.LocalTime

trait UserDB {

	val db : Database

	/*
	 * Users
	 */
	//case class UsersRow(id: Long, name: Option[String] = None, firstname: Option[String] = None, email: String, password: String, address: Option[String] = None, timezone: String = "Europe/Berlin", daystart: LocalTime = new LocalTime(8,0,0,0), dayend: LocalTime = new LocalTime(20,0,0,0))
  
	class Users(tag: Tag) extends Table[User](tag, "users") {
		def * = (id, name, firstname, email, Rep.Some(password), address, timezone, daystart, dayend) <> (User.tupled, User.unapply)

		/** Database column id SqlType(bigserial), AutoInc, PrimaryKey */
		val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
		/** Database column name SqlType(varchar), Default(None) */
		val name: Rep[Option[String]] = column[Option[String]]("name", O.Default(None))
		/** Database column firstname SqlType(varchar), Default(None) */
		val firstname: Rep[Option[String]] = column[Option[String]]("firstname", O.Default(None))
		/** Database column email SqlType(varchar) */
		val email: Rep[String] = column[String]("email")
		/** Database column password SqlType(varchar) */
		val password: Rep[String] = column[String]("password")
		/** Database column address SqlType(varchar), Default(None) */
		val address: Rep[Option[String]] = column[Option[String]]("address", O.Default(None))
		/** Database column timezone SqlType(varchar), Default(Europe/Berlin) */
		val timezone: Rep[String] = column[String]("timezone", O.Default("Europe/Berlin"))
		/** Database column daystart SqlType(int4), Default(480) */
		val daystart: Rep[LocalTime] = column[LocalTime]("daystart")
		/** Database column dayend SqlType(int4), Default(1200) */
		val dayend: Rep[LocalTime] = column[LocalTime]("dayend")

		/** Uniqueness Index over (email) (database name user_email_index) */
		val index1 = index("user_email_index", email, unique=true)
	}

	val users = TableQuery[Users]

	def addUser(user: NewUser): Future[Option[User]] = {
		var action = ((users.map(u => (u.name, u.firstname, u.email, u.password, u.address, u.timezone, u.daystart, u.dayend)) returning users.map(_.id)) += ((user.name, user.firstname, user.email, user.password.get, user.address, user.timezone, user.dayStart, user.dayEnd)))

		db.run(action).map { id =>
			val u = User(
				id,
				user.copy(password = None)
			)

			Some(u)
		}
	}

	def getUser(email: String): Future[Option[User]] = {
		var query = users.filter(u => u.email === email).result.headOption

		db.run(query)
	}

	def getUser(id: Long) : Future[Option[User]] = {
		var query = users.filter(u => u.id === id).result.headOption

		db.run(query)
	}

	def updateUser(oldEmail: String, user: AbstractUser) : Future[Boolean] = {
		if(user.password.isEmpty) { // Keep old password
			var query = users.filter(u => u.email === oldEmail).map(u => (u.name, u.firstname, u.email, u.address, u.timezone, u.daystart, u.dayend))
			var action = query.update((user.name, user.firstname, user.email, user.address, user.timezone, user.dayStart, user.dayEnd))

			var affectedRows : Future[Int] = db.run(action)

			affectedRows.map (_ == 1)
		} else {
			var query = users.filter(u => u.email === oldEmail).map(u => (u.name, u.firstname, u.email, u.password, u.address, u.timezone, u.daystart, u.dayend))
			var action = query.update((user.name, user.firstname, user.email, user.password.get, user.address, user.timezone, user.dayStart, user.dayEnd))

			var affectedRows : Future[Int] = db.run(action)

			affectedRows.map(_ == 1)
		}
	}

	def deleteUser(email: String) : Future[Boolean] = {
		var action = users.filter(u => u.email === email).delete
		var affectedRows : Future[Int] = db.run(action)

		affectedRows.map(_ == 1)
	}

	def authenticate(userPass: Option[UserPass]): Future[Option[User]] = {
	    var f : Option[Future[Option[User]]] = userPass map {up: UserPass => {
	      	var query = users.filter(u => u.email === up.user && u.password === up.pass).result.headOption

			db.run(query)
	      }
	    }

	    f.getOrElse(Future (None))
	}
}
