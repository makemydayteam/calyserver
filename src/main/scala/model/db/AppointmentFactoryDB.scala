package com.example.model.db

import com.github.tminglei.slickpg.`[_,_]`
import com.github.tminglei.slickpg.Range
import com.example.model.MyPostgresDriver.api._
import slick.model.ForeignKeyAction
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import spray.json._
import DefaultJsonProtocol._

import com.example.model.{AppointmentFactory, AppointmentExceptionalAttributes, LocationRef}

import org.joda.time.{DateTime, Duration, Period, Interval}

trait AppointmentFactoryDB extends UserDB with LocationDB {

	val db : Database

	case class AppointmentFactoriesRow(
		id: Long,
		owner: Long,
		name: String,
		location: Option[Long] = None,
		description: Option[String] = None,
		tags: Option[JsValue] = None,
		intervalFactory: Option[Long] = None
	)

	/** Table description of table appointment_factories. Objects of this class serve as prototypes for rows in queries. */
	class AppointmentFactories(_tableTag: Tag) extends Table[AppointmentFactoriesRow](_tableTag, "appointment_factories") {
    	def * = (id, owner, name, location, description, tags, intervalFactory) <> (AppointmentFactoriesRow.tupled, AppointmentFactoriesRow.unapply)

    	/** Database column id SqlType(bigserial), AutoInc, PrimaryKey */
    	val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
	    /** Database column owner SqlType(int8) */
	    val owner: Rep[Long] = column[Long]("owner")
	    /** Database column name SqlType(varchar) */
	    val name: Rep[String] = column[String]("name")
	    /** Database column location SqlType(int8), Default(None) */
	    val location: Rep[Option[Long]] = column[Option[Long]]("location", O.Default(None))
	    /** Database column description SqlType(varchar), Default(None) */
	    val description: Rep[Option[String]] = column[Option[String]]("description", O.Default(None))
	    /** Database column tags SqlType(json), Length(2147483647,false), Default(None) */
	    val tags: Rep[Option[JsValue]] = column[Option[JsValue]]("tags", O.Length(2147483647,varying=false), O.Default(None))
	    /** Database column interval_factory SqlType(int8), Default(None) */
	    val intervalFactory: Rep[Option[Long]] = column[Option[Long]]("interval_factory", O.Default(None))

	    /** Foreign key referencing AppointmentIntervalFactory (database name appointment_factories_interval_factory_fkey) */
	    lazy val appointmentIntervalFactoryFk = foreignKey("appointment_factories_interval_factory_fkey", intervalFactory, appointmentIntervalFactory)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
	    /** Foreign key referencing Locations (database name appointment_factories_location_fkey) */
	    lazy val locationsFk = foreignKey("appointment_factories_location_fkey", location, locations)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
	    /** Foreign key referencing Users (database name appointment_factories_owner_fkey) */
	    lazy val usersFk = foreignKey("appointment_factories_owner_fkey", owner, users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
  	}

  	/** Collection-like TableQuery object for table AppointmentFactories */
  	lazy val appointmentFactories = new TableQuery(tag => new AppointmentFactories(tag))

	case class AppointmentIntervalFactoryRow(
		id: Long,
		starttime: DateTime,
		endtime: Option[DateTime] = None,
		period: Period,
		duration: Period
	) {
		def toAppointmentPeriodFactory : com.example.model.AppointmentPeriodFactory = {
			com.example.model.AppointmentPeriodFactory(Some(id), starttime, endtime, period, duration)
		}
	}

	/** Table description of table appointment_interval_factory. Objects of this class serve as prototypes for rows in queries. */
	class AppointmentIntervalFactory(_tableTag: Tag) extends Table[AppointmentIntervalFactoryRow](_tableTag, "appointment_interval_factory") {
		def * = (id, starttime, endtime, period, duration) <> (AppointmentIntervalFactoryRow.tupled, AppointmentIntervalFactoryRow.unapply)

		/** Database column id SqlType(bigserial), AutoInc, PrimaryKey */
		val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
		/** Database column starttime SqlType(timestamp), Default(None) */
		val starttime: Rep[DateTime] = column[DateTime]("starttime")
		/** Database column endtime SqlType(timestamp), Default(None) */
		val endtime: Rep[Option[DateTime]] = column[Option[DateTime]]("endtime", O.Default(None))
		/** Database column period SqlType(varchar), Default(None) */
		val period: Rep[Period] = column[Period]("period")
		/** Database column duration SqlType(varchar), Default(None) */
		val duration: Rep[Period] = column[Period]("duration")
	}

	/** Collection-like TableQuery object for table AppointmentIntervalFactory */
	lazy val appointmentIntervalFactory = new TableQuery(tag => new AppointmentIntervalFactory(tag))

	case class AppointmentListFactoryRow(id: Long, time: Range[DateTime])

	/** Table description of table appointment_list_factory. Objects of this class serve as prototypes for rows in queries. */
	class AppointmentListFactory(_tableTag: Tag) extends Table[AppointmentListFactoryRow](_tableTag, "appointment_list_factory") {
		def * = (id, time) <> (AppointmentListFactoryRow.tupled, AppointmentListFactoryRow.unapply)

		/** Database column id SqlType(int8) */
		val id: Rep[Long] = column[Long]("id")
		/** Database column time SqlType(tstzrange), Length(2147483647,false) */
		val time: Rep[Range[DateTime]] = column[Range[DateTime]]("time", O.Length(2147483647,varying=false))

		/** Primary key of AppointmentListFactory (database name appointment_list_factory_pkey) */
		val pk = primaryKey("appointment_list_factory_pkey", (id, time))

		/** Foreign key referencing AppointmentFactories (database name appointment_list_factory_id_fkey) */
		lazy val appointmentFactoriesFk = foreignKey("appointment_list_factory_id_fkey", id, appointmentFactories)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
	}

	/** Collection-like TableQuery object for table AppointmentListFactory */
	lazy val appointmentListFactory = new TableQuery(tag => new AppointmentListFactory(tag))

	case class AppointmentExceptionsRow(
		id: Long,
		oldTime: DateTime,
		time: Option[Range[DateTime]] = None,
		location: Option[Long] = None)

	class AppointmentExceptions(_tableTag: Tag) extends Table[AppointmentExceptionsRow](_tableTag, "appointment_exceptions") {
		def * = (id, oldTime, time, location) <> (AppointmentExceptionsRow.tupled, AppointmentExceptionsRow.unapply)

		/** Database column id SqlType(int8) */
		val id: Rep[Long] = column[Long]("id")
		/** Database column old_time SqlType(timestamptz) */
		val oldTime: Rep[DateTime] = column[DateTime]("old_time")
		/** Database column time SqlType(tstzrange), Length(2147483647,false), Default(None) */
		val time: Rep[Option[Range[DateTime]]] = column[Option[Range[DateTime]]]("time", O.Default(None))
		/** Database column location SqlType(int8), Default(None) */
		val location: Rep[Option[Long]] = column[Option[Long]]("location", O.Default(None))

		/** Primary key of AppointmentExceptions (database name appointment_exceptions_pkey) */
		val pk = primaryKey("appointment_exceptions_pkey", (id, oldTime))

		/** Foreign key referencing AppointmentFactories (database name appointment_exceptions_id_fkey) */
		lazy val appointmentFactoriesFk = foreignKey("appointment_exceptions_id_fkey", id, appointmentFactories)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
		/** Foreign key referencing Locations (database name appointment_exceptions_location_fkey) */
		lazy val locationsFk = foreignKey("appointment_exceptions_location_fkey", location, locations)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
	}

	/** Collection-like TableQuery object for table AppointmentExceptions */
	lazy val appointmentExceptions = new TableQuery(tag => new AppointmentExceptions(tag))

	/*
	 * Appointments
	 */

	def addAppointmentFactory(fac: AppointmentFactory): Future[Option[AppointmentFactory]] = {

		fac.factory match {
			case (l : com.example.model.AppointmentListFactory) =>
				val action = ((appointmentFactories returning appointmentFactories.map(_.id))
					  += AppointmentFactoriesRow(-1, fac.owner.get, fac.name, fac.location.map(_.id), fac.description, Some(fac.tags.toJson), None) )

				db.run(action).flatMap { id =>
					addExceptionalAttributes(id, fac.exceptions).flatMap {
						case true =>
							addListFactory(id, l).map { s =>
								if(s)
									Some(fac.copy(id = Some(id)))
								else
									None
							}

						case _ => Future(None)
					}
				}

			case (p: com.example.model.AppointmentPeriodFactory) =>
				addPeriodFactory(p).flatMap { f =>
					val action = ((appointmentFactories returning appointmentFactories.map(_.id))
							  += AppointmentFactoriesRow(-1, fac.owner.get, fac.name, fac.location.map(_.id), fac.description, Some(fac.tags.toJson), f.get.id) )

					db.run(action).flatMap { id =>
						addExceptionalAttributes(id, fac.exceptions).map {
							case true =>
								val newFac = fac.copy(id = Some(id))
								Some(newFac)

							case _ => None
						}
					}
				}
		}
	}

	def addListFactory(id: Long, m: com.example.model.AppointmentListFactory) : Future[Boolean] = {
		val action = (appointmentListFactory ++=
			m.list.map {
				t => AppointmentListFactoryRow(
						id,
						Range(t.getStart, t.getEnd, `[_,_]`)
					)
			})

		db.run(action).map {
			case Some(affectedRows) =>
				println(s"affectedRows: ${affectedRows}")
				affectedRows == m.list.size

			case _ => false
		}
	}

	def addPeriodFactory(p: com.example.model.AppointmentPeriodFactory) : Future[Option[com.example.model.AppointmentPeriodFactory]] = {
		val action = ((appointmentIntervalFactory returning appointmentIntervalFactory.map(_.id))
				  += AppointmentIntervalFactoryRow(-1, p.starttime, p.endtime, p.period, p.duration))

		db.run(action).map { id =>
			Some(p.copy(id = Some(id)))
		}
	}

	def updatePeriodFactory(p: com.example.model.AppointmentPeriodFactory) : Future[Option[com.example.model.AppointmentPeriodFactory]] = {
		val query = appointmentIntervalFactory.filter(_.id === p.id).map(p => (p.starttime, p.endtime, p.period, p.duration))

		val action = query.update((p.starttime, p.endtime, p.period, p.duration))

		db.run(action).map {n =>
			if (n > 0)
				Some(p)
			else
				None
		}
	}

	def _deletePeriodFactory(id: Long) : Future[Boolean] = {
		val action = appointmentIntervalFactory.filter(a => a.id === id).delete

		db.run(action).map(_ > 0)
	}

	def getAppointmentFactory(id: Long): Future[Option[AppointmentFactory]] = {
		val f = for {
			((f, l), p) <- appointmentFactories.filter(f => f.id === id) joinLeft locations on (_.location === _.id) joinLeft appointmentIntervalFactory on (_._1.intervalFactory === _.id)
		} yield (f, l, p)

		db.run(f.result.headOption).flatMap {
			case Some((f, l, Some(p))) =>
				_getExceptionalAttributes(id).map { e =>
					Some(AppointmentFactory(
						Some(f.id),
						Some(f.owner),
						f.name,
						f.tags.map(_.convertTo[Seq[String]]).getOrElse(Seq()),
						l,
						f.description,
						p.toAppointmentPeriodFactory,
						e
					))
				}

			case Some((f, l, None)) =>
				_getExceptionalAttributes(id).flatMap { e =>
					_getListFactory(id).map { fac =>
						Some(AppointmentFactory(
							Some(f.id),
							Some(f.owner),
							f.name,
							f.tags.map(_.convertTo[Seq[String]]).getOrElse(Seq()),
							l,
							f.description,
							fac,
							e)
						)
					}
				}

			case _ => Future(None)
		}
	}

	def getAppointmentFactoryByOwner(userid: Long): Future[Seq[AppointmentFactory]] = {
		val f = for {
			((f, l), p) <- appointmentFactories.filter(f => f.owner === userid) joinLeft locations on (_.location === _.id) joinLeft appointmentIntervalFactory on (_._1.intervalFactory === _.id)
		} yield (f, l, p)

		db.run(f.result).flatMap { facs =>
			Future.sequence {
				facs map {
					case (f, l, Some(p)) =>
						_getExceptionalAttributes(f.id).map { e =>
							AppointmentFactory(
								Some(f.id),
								Some(f.owner),
								f.name,
								f.tags.map(_.convertTo[Seq[String]]).getOrElse(Seq()),
								l,
								f.description,
								p.toAppointmentPeriodFactory,
								e
							)
						}

					case (f, l, None) =>
						_getExceptionalAttributes(f.id).flatMap { e =>
							_getListFactory(f.id).map { fac =>
								AppointmentFactory(
									Some(f.id),
									Some(f.owner),
									f.name,
									f.tags.map(_.convertTo[Seq[String]]).getOrElse(Seq()),
									l,
									f.description,
									fac,
									e
									)
							}
						}
				}
			}
		}
	}

	def _getListFactory(id: Long) : Future[com.example.model.AppointmentListFactory] = {
		val query = appointmentListFactory.filter(l => l.id === id).result

		db.run(query).map { l =>
			com.example.model.AppointmentListFactory(l.map { r =>
				new Interval(r.time.start, r.time.end)
			})
		}
	}

	def _deleteListFactory(id: Long) : Future[Boolean] = {
		val action = appointmentListFactory.filter(a => a.id === id).delete

		db.run(action).map(_ => true)
	}

	def _updateAppointmentFactory(fac: AppointmentFactory) : Future[Option[AppointmentFactory]] = {
		val query = appointmentFactories.filter(_.id === fac.id).map(f => (f.name, f.location, f.description, f.tags))

		val action = query.update((fac.name, fac.location.map(_.id), fac.description, Some(fac.tags.toJson)))

		db.run(action).map(n => if (n > 0) Some(fac) else None)
	}

	def updateAppointmentFactory(fac: AppointmentFactory) : Future[Option[AppointmentFactory]] = {
		fac.factory match {
			case (l : com.example.model.AppointmentListFactory) =>
				_updateAppointmentFactory(fac).flatMap {
					case Some(newFac) =>
						_deleteExceptionalAttributes(newFac.id.get).flatMap {
							case true =>
								addExceptionalAttributes(newFac.id.get, fac.exceptions).flatMap {
									case true =>
										_deleteListFactory(newFac.id.get).flatMap {
											case true =>
												addListFactory(newFac.id.get, l).map { s =>
													if(s)
														Some(newFac)
													else
														None
												}

											case _ => Future(None)
										}

									case _ => Future(None)
								}

							case _ => Future(None)
						}

					case _ => Future(None)
				}

			case (p: com.example.model.AppointmentPeriodFactory) =>
				updatePeriodFactory(p).flatMap { f =>
					_updateAppointmentFactory(fac).flatMap {
						case Some(newFac) =>
							_deleteExceptionalAttributes(newFac.id.get).flatMap {
								case true =>
									addExceptionalAttributes(newFac.id.get, fac.exceptions).map {
										case true =>
											Some(newFac)

										case _ => None
									}

								case _ => Future(None)
							}

						case _ => Future(None)
					}
				}
		}
	}

	def deleteAppointmentFactory(id: Long) : Future[Boolean] = {
		val action = appointmentFactories.filter(a => a.id === id).delete

		getAppointmentFactory(id) flatMap {
			case Some(fac) =>
				fac.factory match {
					case (l: com.example.model.AppointmentListFactory) =>
						db.run(action).map(_ > 0)

					case (p: com.example.model.AppointmentPeriodFactory) =>
						_deletePeriodFactory(p.id.get).flatMap {
							case true => db.run(action).map(_ > 0)
							case _ => Future(false)
						}
				}

			case None => Future(false)
		}
	}

	def addExceptionalAttributes(id: Long, e: Seq[AppointmentExceptionalAttributes]) : Future[Boolean] = {
		val action = (appointmentExceptions ++=
			e.map {
				e => AppointmentExceptionsRow(
						id,
						e.oldTime,
						e.time.map(t => Range(t.getStart, t.getEnd, `[_,_]`)),
						e.location.map(_.id))
			})

		db.run(action).map {
			case Some(affectedRows) =>
				affectedRows == e.size

			case _ => false
		}
	}

	def _getExceptionalAttributes(id: Long) : Future[Seq[AppointmentExceptionalAttributes]] = {
		val query =  (appointmentExceptions.filter(l => l.id === id) joinLeft locations on (_.location === _.id) ).result

		db.run(query).map { _.map { case (e, l) =>
				AppointmentExceptionalAttributes(e.oldTime, e.time.map(r => new Interval(r.start, r.end)), l)
			}
		}
	}

	def _deleteExceptionalAttributes(id: Long) : Future[Boolean] = {
		val action = appointmentExceptions.filter(a => a.id === id).delete

		db.run(action).map(_ => true)
	}
}
