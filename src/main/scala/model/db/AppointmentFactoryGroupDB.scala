package com.example.model.db

import com.example.model.MyPostgresDriver.api._
import slick.model.ForeignKeyAction
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import spray.json._
import DefaultJsonProtocol._

import com.example.model.{InstitutionRow, AppointmentGroupsRow, AppointmentFactory, AppointmentGroup}

trait AppointmentFactoryGroupDB extends UserDB with AppointmentFactoryDB {

  val db : Database

  def getAllInstitutions() : Future[Seq[InstitutionRow]] = {
    val query = institutions.result
    db.run(query)
  }

  class Institutions(_tableTag: Tag) extends Table[InstitutionRow](_tableTag, "institutions") {
    def * = (id, university, faculty) <> (InstitutionRow.tupled, InstitutionRow.unapply)

    val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    val university: Rep[String] = column[String]("university")
    val faculty: Rep[String] = column[String]("faculty")
  }

  lazy val institutions = new TableQuery(tag => new Institutions(tag))

  class AppointmentGroups(_tableTag: Tag) extends Table[AppointmentGroupsRow](_tableTag, "appointment_groups") {
    def * = (id, institution, name) <> (AppointmentGroupsRow.tupled, AppointmentGroupsRow.unapply)

    val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    val institution: Rep[Long] = column[Long]("institution")
    val name: Rep[String] = column[String]("name")

    lazy val institutionsFk = foreignKey("appointment_groups_institution_fkey", institution, institutions)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
  }

  lazy val appointmentGroups = new TableQuery(tag => new AppointmentGroups(tag))

  def getGroups(userId: Long, institutionId : Long) : Future[Seq[AppointmentGroup]] = {
    val query = (appointmentGroups.filter(_.institution === institutionId) joinLeft usersAppointmentGroups.filter(_.userId === userId) on (_.id === _.appointmentGroupId)).result

    db.run(query).map { _ map {
      case (r : AppointmentGroupsRow, u : Option[UsersAppointmentGroupsRow]) => AppointmentGroup(r.id, r.institution, r.name, u.isDefined)
    }}
  }

  case class AppointmentGroupItemRow(id: Long, appointmentId: Long)

  class AppointmentGroupItems(_tableTag: Tag) extends Table[AppointmentGroupItemRow](_tableTag, "appointment_group_items") {
    def * = (id, appointmentId) <> (AppointmentGroupItemRow.tupled, AppointmentGroupItemRow.unapply)

    val id: Rep[Long] = column[Long]("id")
    val appointmentId: Rep[Long] = column[Long]("appointment_id")

    lazy val idsFk = foreignKey("appointment_group_items_id_fkey", id, appointmentGroups)(g => g.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
    lazy val appointmentIdsFk = foreignKey("appointment_group_items_appointment_id_fkey", appointmentId, appointmentFactories)(f => f.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
    val pk = primaryKey("appointment_groups_items_pkey", (id, appointmentId))
  }

  lazy val appointmentGroupItems = new TableQuery(tag => new AppointmentGroupItems(tag))

  def getGroupAppointments(appointmentGroupId: Long) : Future[Seq[AppointmentFactory]] = {
    val query = appointmentGroupItems.filter(_.id === appointmentGroupId).result

    db.run(query).map {
      _ map (r => getAppointmentFactory(r.appointmentId) )
    }.flatMap { Future.sequence(_).map(_.filter(_.isDefined).map(_.get)) }
  }

  case class UsersAppointmentGroupsRow(userId: Long, appointmentGroupId: Long)

  class UsersAppointmentGroups(_tableTag: Tag) extends Table[UsersAppointmentGroupsRow](_tableTag, "users_appointment_groups") {
    def * = (userId, appointmentGroupId) <> (UsersAppointmentGroupsRow.tupled, UsersAppointmentGroupsRow.unapply)

    val userId: Rep[Long] = column[Long]("user_id")
    val appointmentGroupId: Rep[Long] = column[Long]("appointment_group_id")

    lazy val userIdsFk = foreignKey("users_appointment_groups_user_id_fkey", userId, users)(u => u.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
    lazy val appointmentGroupIdsFk = foreignKey("users_appointment_groups_appointment_group_id_fkey", appointmentGroupId, appointmentGroups)(g => g.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
    val pk = primaryKey("users_appointment_groups_pkey", (userId, appointmentGroupId))
  }

  lazy val usersAppointmentGroups = new TableQuery(tag => new UsersAppointmentGroups(tag))

  def addAppointmentGroupToUser(userId: Long, appointmentGroupId: Long) : Future[Boolean] = {
    val action = usersAppointmentGroups += UsersAppointmentGroupsRow(userId, appointmentGroupId)

    db.run(action).map { n =>
      n == 1
    }
  }

  def removeAppointmentGroupFromUser(userId: Long, appointmentGroupId: Long) : Future[Boolean] = {
    val action = usersAppointmentGroups.filter(u => u.userId === userId && u.appointmentGroupId === appointmentGroupId).delete

    db.run(action).map { n =>
      n == 1
    }
  }

  def getAppointmentGroupFactoriesByUser(userId: Long) : Future[Seq[AppointmentFactory]] = {
    val query = usersAppointmentGroups.filter(_.userId === userId).result

    val groups : Future[Seq[UsersAppointmentGroupsRow]] = db.run(query)

    val appointmentFactories : Future[Seq[Future[Seq[AppointmentFactory]]]] = groups.map {
      _.map(g => getGroupAppointments(g.appointmentGroupId))
    }

    appointmentFactories.flatMap {
      Future.sequence(_).map { _.flatten}
    }
  }

}
