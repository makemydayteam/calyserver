package com.example.model.db

import com.github.tminglei.slickpg.`[_,_]`
import com.github.tminglei.slickpg.Range
import com.example.model.MyPostgresDriver.api._
import slick.model.ForeignKeyAction
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import spray.json._
import DefaultJsonProtocol._

import com.example.model.{Hangout, AbstractUser, IdentifiableUser, User, LocalTimeInterval}

import org.joda.time.{DateTime, Period, Interval, LocalTime}

trait HangoutDB extends UserDB with LocationDB {

	case class HangoutsRow(
		id: Long,
		owner: Long,
		name: String,
		location: Option[Long] = None,
		description: Option[String] = None,
		duration: Period,
		schedulePeriod: Option[Range[DateTime]] = None,
		scheduleDayTimeStart: Option[LocalTime] = None,
		scheduleDayTimeEnd: Option[LocalTime] = None,
		tags: JsValue,
		time: Range[DateTime])

	class Hangouts(_tableTag: Tag) extends Table[HangoutsRow](_tableTag, "hangouts") {
    	def * = (id, owner, name, location, description, duration, schedulePeriod, scheduleDayTimeStart, scheduleDayTimeEnd, tags, time) <> (HangoutsRow.tupled, HangoutsRow.unapply)

		/** Database column id SqlType(bigserial), AutoInc, PrimaryKey */
		val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
		/** Database column owner SqlType(int8) */
		val owner: Rep[Long] = column[Long]("owner")
		/** Database column name SqlType(varchar) */
		val name: Rep[String] = column[String]("name")
		/** Database column location SqlType(int8), Default(None) */
		val location: Rep[Option[Long]] = column[Option[Long]]("location", O.Default(None))
		/** Database column description SqlType(varchar), Default(None) */
		val description: Rep[Option[String]] = column[Option[String]]("description", O.Default(None))
		/** Database column duration SqlType(int4) */
		val duration: Rep[Period] = column[Period]("duration")
		/** Database column schedule_period SqlType(tstzrange), Length(2147483647,false), Default(None) */
		val schedulePeriod: Rep[Option[Range[DateTime]]] = column[Option[Range[DateTime]]]("schedule_period", O.Default(None))
		val scheduleDayTimeStart: Rep[Option[LocalTime]] = column[Option[LocalTime]]("schedule_day_time_start", O.Default(None))
		val scheduleDayTimeEnd: Rep[Option[LocalTime]] = column[Option[LocalTime]]("schedule_day_time_end", O.Default(None))
		/** Database column tags SqlType(json), Length(2147483647,false) */
		val tags: Rep[JsValue] = column[JsValue]("tags")
		/** Database column time SqlType(tstzrange), Length(2147483647,false) */
		val time: Rep[Range[DateTime]] = column[Range[DateTime]]("time")

		/** Foreign key referencing Locations (database name hangouts_location_fkey) */
		lazy val locationsFk = foreignKey("hangouts_location_fkey", location, locations)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
		/** Foreign key referencing Users (database name hangouts_owner_fkey) */
		lazy val usersFk = foreignKey("hangouts_owner_fkey", owner, users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
	}

	/** Collection-like TableQuery object for table Hangouts */
	val hangouts = new TableQuery(tag => new Hangouts(tag))

	case class AttendeesRow(hangoutId: Long, userId: Long, accepted: Option[Boolean])

	/** Table description of table attendees. Objects of this class serve as prototypes for rows in queries. */
	class Attendees(_tableTag: Tag) extends Table[AttendeesRow](_tableTag, "attendees") {
		def * = (hangoutId, userId, accepted) <> (AttendeesRow.tupled, AttendeesRow.unapply)

		/** Database column hangout_id SqlType(int8) */
		val hangoutId: Rep[Long] = column[Long]("hangout_id")
		/** Database column user_id SqlType(int8) */
		val userId: Rep[Long] = column[Long]("user_id")

		val accepted: Rep[Option[Boolean]] = column[Option[Boolean]]("accepted", O.Default(None))

		/** Primary key of Attendees (database name attendees_pkey) */
		val pk = primaryKey("attendees_pkey", (hangoutId, userId))

		/** Foreign key referencing Hangouts (database name attendees_hangout_id_fkey) */
		lazy val hangoutsFk = foreignKey("attendees_hangout_id_fkey", hangoutId, hangouts)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
		/** Foreign key referencing Users (database name attendees_user_id_fkey) */
		lazy val usersFk = foreignKey("attendees_user_id_fkey", userId, users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
	}
	/** Collection-like TableQuery object for table Attendees */
	val attendees = new TableQuery(tag => new Attendees(tag))

	/*
	 * Attendees
	 */
	def addAttendees(hangoutId: Long, attendee: Seq[IdentifiableUser]) : Future[Boolean] = {
		val action = (attendees ++=
			attendee.map {
				a => AttendeesRow(hangoutId, a.id, None)
			})

		db.run(action).map {
			case Some(affectedRows) =>
				affectedRows == attendee.size

			case _ => false
		}
	}

	def getAttendees(hangoutId: Long) : Future[Seq[IdentifiableUser]] = {
		val query = attendees.filter(a => a.hangoutId === hangoutId) join users on (_.userId === _.id)

		db.run(query.result).map { _.map {
			case (_ : AttendeesRow, u : User) =>
				u
			}
		}
	}

	def deleteAttendees(hangoutId: Long) : Future[Boolean] = {
		val action = attendees.filter(a => a.hangoutId === hangoutId).delete

		db.run(action).map (_ => true)
	}

	def deleteAttendee(hangoutId: Long, attendeeId: Long) : Future[Boolean] = {
		val action = attendees.filter(a => a.hangoutId === hangoutId && a.userId === attendeeId).delete

		db.run(action).map (_ => true)
	}

	/*
	 * Hangout
	 */
	def addHangout(hangout: Hangout) : Future[Option[Hangout]] = {
		val action = ((hangouts returning hangouts.map(_.id))
				  += HangoutsRow(-1, hangout.owner.get, hangout.name, hangout.location.map(_.id), hangout.description, hangout.duration, hangout.timeFence.map(i => Range(i.getStart, i.getEnd, `[_,_]`)), hangout.dayTime.map(_.start), hangout.dayTime.map(_.end), hangout.tags.toJson, Range(hangout.time.get.getStart, hangout.time.get.getEnd, `[_,_]`)) )

		db.run(action).flatMap { id =>
			addAttendees(id, hangout.attendees).map {
				case true =>
					Some(hangout.copy(id = Some(id)))

				case _ => None
			}
		}

	}

	def acceptHangout(userId: Long, hangoutId: Long) : Future[Boolean] = {
		val query = for {a <- attendees if a.hangoutId === hangoutId && a.userId === userId}
			yield a.accepted

		val action = query.update(Some(true))

		db.run(action).map {
			_ == 1
		}
	}

	def getAcceptanceStateHangout(userId: Long, hangoutId: Long) : Future[Option[Boolean]] = {
		val query =
			for {
				a <- attendees if a.hangoutId === hangoutId && a.userId === userId
			} yield a.accepted

		db.run(query.result.headOption).map{_.flatten}
	}

	def rejectHangout(userId: Long, hangoutId: Long) : Future[Boolean] = {
		val query = for {a <- attendees if a.hangoutId === hangoutId && a.userId === userId}
			yield a.accepted

		val action = query.update(Some(false))

		db.run(action).map {
			_ == 1
		}
	}

	def clearHangoutAcceptance(hangoutId: Long) : Future[Boolean] = {
		val query = for {a <- attendees if a.hangoutId === hangoutId}
			yield a.accepted

		val action = query.update(None)

		db.run(action).map {
			_ => true
		}
	}

	def updateHangoutTime(hangoutId: Long, time: Interval) : Future[Boolean] = {
		val query = for {h <- hangouts if h.id === hangoutId}
			yield h.time

		val action = query.update(Range(time.getStart, time.getEnd, `[_,_]`))

		db.run(action).map {
			_ == 1 // one row affected
		}
	}

	def getHangout(id: Long) : Future[Option[Hangout]] = {
		val query = hangouts.filter(_.id === id) joinLeft locations on (_.location === _.id)

		db.run(query.result.headOption).flatMap {
			case Some((h, l)) =>
				getAttendees(id).map { att =>
					val scheduleDayTime =
						for {
							s <- h.scheduleDayTimeStart
							e <- h.scheduleDayTimeEnd
						} yield LocalTimeInterval(s,e)

					Some(Hangout(Some(h.id), Some(h.owner), h.name, l, h.description, h.duration, h.schedulePeriod.map(r => new Interval(r.start, r.end)), scheduleDayTime, h.tags.convertTo[Seq[String]], Some(new Interval(h.time.start, h.time.end)), att))
				}

			case _ => Future(None)
		}
	}

	def _getHangoutByAttendee(attendee: Long, interval: Interval) : Future[Seq[Hangout]] = {
		val query = attendees.filter(_.userId === attendee) join hangouts.filter(_.time @& Range(interval.getStart, interval.getEnd).bind) on (_.hangoutId === _.id) joinLeft locations on (_._2.location === _.id)

		db.run(query.result).map { _ map {
			case ((a,h), l) =>
				val scheduleDayTime =
					for {
						s <- h.scheduleDayTimeStart
						e <- h.scheduleDayTimeEnd
					} yield LocalTimeInterval(s,e)

				Hangout(Some(h.id), Some(h.owner), h.name, l, h.description, h.duration, h.schedulePeriod.map(r => new Interval(r.start, r.end)), scheduleDayTime, h.tags.convertTo[Seq[String]], Some(new Interval(h.time.start, h.time.end)), Seq(), Some(false), a.accepted)
		} }
	}

	def _getHangoutByOwner(owner: Long, interval: Interval) : Future[Seq[Hangout]] = {
		val query = hangouts.filter(h => h.owner === owner && h.time @& Range(interval.getStart, interval.getEnd).bind) joinLeft locations on (_.location === _.id)

		db.run(query.result).map { _ map {
			case (h, l) =>
				val scheduleDayTime =
					for {
						s <- h.scheduleDayTimeStart
						e <- h.scheduleDayTimeEnd
					} yield LocalTimeInterval(s,e)

				Hangout(Some(h.id), Some(h.owner), h.name, l, h.description, h.duration, h.schedulePeriod.map(r => new Interval(r.start, r.end)), scheduleDayTime, h.tags.convertTo[Seq[String]], Some(new Interval(h.time.start, h.time.end)), Seq(), Some(true))
		} }
	}

	def getHangout(attendee: Long, interval: Interval) : Future[Seq[Hangout]] = {
		val hangouts = _getHangoutByAttendee(attendee, interval).flatMap { a =>
			_getHangoutByOwner(attendee, interval).map { o =>
				a ++ o
			}
		}

		hangouts.flatMap { seq =>
			val s = for {
				h <- seq
			} yield getAttendees(h.id.get).map { att =>
				h.copy(attendees = att)
			}

			Future.sequence(s)
		}
	}

	def updateHangout(app: Hangout) : Future[Option[Hangout]] = Future(None)

	def deleteHangout(id: Long) : Future[Boolean] = {
		val action = hangouts.filter(h => h.id === id).delete

		db.run(action).map(_ > 0)
	}
}
