package com.example.model.db

import com.github.tminglei.slickpg.`[_,_]`
import com.github.tminglei.slickpg.Range
import com.example.model.MyPostgresDriver.api._
import slick.model.ForeignKeyAction
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import spray.json._
import DefaultJsonProtocol._

import com.example.model.{Todo, Location, PlannedTodo}

import org.joda.time.{DateTime, Period, Interval}

trait TodoDB extends UserDB with LocationDB {

	val db : Database

	/*
	 * Todos
	 */
	case class TodosRow(
		id: Long,
		owner: Long,
		name: String,
		duration: Period,
		timeFence: Option[Range[DateTime]],
		executionStart: Option[DateTime],
		executionEnd: Option[DateTime],
		location: Option[Long],
		description: Option[String],
		tags: JsValue,
		plannedTime: Range[DateTime])

	class Todos(tag: Tag) extends Table[TodosRow](tag, "todos") {
		def * = (id, owner, name, duration, timeFence, executionStart, executionEnd, location, description, tags, plannedTime) <> (TodosRow.tupled, TodosRow.unapply)
		
		/** Database column id SqlType(int8), PrimaryKey */
		val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
		/** Database column owner SqlType(int8) */
		val owner: Rep[Long] = column[Long]("owner")
		/** Database column name SqlType(varchar) */
		val name: Rep[String] = column[String]("name")
		/** Database column duration SqlType(int4), Default(None) */
		val duration: Rep[Period] = column[Period]("duration")
		/** Database column schedule_period SqlType(tstzrange), Length(2147483647,false), Default(None) */
		val timeFence: Rep[Option[Range[DateTime]]] = column[Option[Range[DateTime]]]("time_fence")
		/** Database column execution_time */
		val executionStart: Rep[Option[DateTime]] = column[Option[DateTime]]("execution_start")
		/** Database column execution_time */
		val executionEnd: Rep[Option[DateTime]] = column[Option[DateTime]]("execution_end")
		/** Database column location SqlType(int8), Default(None) */
		val location: Rep[Option[Long]] = column[Option[Long]]("location", O.Default(None))
		/** Database column description SqlType(varchar), Default(None) */
		val description: Rep[Option[String]] = column[Option[String]]("description", O.Default(None))
		/** Database column tags SqlType(json), Length(2147483647,false), Default(None) */
		val tags: Rep[JsValue] = column[JsValue]("tags")
		/** Database column planned_time SqlType(tstzrange), Length(2147483647,false), Default(None) */
		val plannedTime: Rep[Range[DateTime]] = column[Range[DateTime]]("planned_time")

		/** Foreign key referencing Locations (database name todos_location_fkey) */
		lazy val locationsFk1 = foreignKey("todos_location_fkey", location, locations)(r => Rep.Some(r.id), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
		/** Foreign key referencing Users (database name todos_owner_fkey) */
		lazy val usersFk = foreignKey("todos_owner_fkey", owner, users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
	}

	val todos = TableQuery[Todos]

	def addTodo(todo: PlannedTodo): Future[Option[Todo]] = {
		var action = ((todos returning todos.map(_.id))
			      += TodosRow(
					-1,
					todo.owner,
					todo.name,
					todo.duration.toStandardSeconds.toPeriod,
					todo.timeFence.map(t => Range(t.getStart, t.getEnd)),
					None,
					None,
					todo.location.map(_.id),
					todo.description,
					todo.tags.toJson,
					Range(todo.plannedTime.getStart, todo.plannedTime.getEnd))
			      )

		db.run(action).map(id => Some(Todo(id, todo)))
	}

	def getTodo(id: Long): Future[Option[Todo]] = {
		var j = for {
			(t,l) <- todos.filter(t => t.id === id) joinLeft locations on (_.location === _.id)
		} yield (t, l)

		var query = j.result.headOption

		db.run(query).map(_ map {j => {
			var t : TodosRow = j._1
			var l: Option[Location] = j._2

			Todo(
				t.id,
				t.owner,
				t.name,
				t.duration.toStandardDuration,
				t.timeFence.map(t => new Interval(t.start, t.end)),
				t.executionStart,
				t.executionEnd,
				l,
				t.description,
				t.tags.convertTo[Seq[String]],
				new Interval(t.plannedTime.start, t.plannedTime.end))
		}})
	}

	def updateTodo(todo: Todo) : Future[Option[Todo]] = {
		val todoRow = TodosRow(
					todo.id,
					todo.owner,
					todo.name,
					todo.duration.toStandardSeconds.toPeriod,
					todo.timeFence.map(t => Range(t.getStart, t.getEnd)),
					todo.executionStart,
					todo.executionEnd,
					todo.location.map(_.id),
					todo.description,
					todo.tags.toJson,
					Range(todo.plannedTime.getStart, todo.plannedTime.getEnd)
			      )

		var query = todos.filter(t => t.id === todo.id && t.owner === todo.owner)
		
		var action = query.update(todoRow)

		var affectedRows : Future[Int] = db.run(action)

		affectedRows.map(n => if (n > 0) Some(todo) else None)
	}

	def deleteTodo(id: Long) : Future[Boolean] = {
		var action = todos.filter(t => t.id === id).delete
		var affectedRows : Future[Int] = db.run(action)

		affectedRows.map(_ > 0)
	}

	def getOverdueTodos(userid : Long, currTime: DateTime) : Future[Seq[Todo]] = {
		var j = for {
			(t,l) <- todos.filter(t => t.owner === userid && t.plannedTime << Range(currTime, currTime, `[_,_]`) && !t.executionEnd.isDefined) joinLeft locations on (_.location === _.id)
		} yield (t, l)

		var query = j.result

		db.run(query).map(_ map {j => {
			var t : TodosRow = j._1
			var l: Option[Location] = j._2

			Todo(
				t.id,
				t.owner,
				t.name,
				t.duration.toStandardDuration,
				t.timeFence.map(t => new Interval(t.start, t.end)),
				t.executionStart,
				t.executionEnd,
				l,
				t.description,
				t.tags.convertTo[Seq[String]],
				new Interval(t.plannedTime.start, t.plannedTime.end))
		}})
	}

}
