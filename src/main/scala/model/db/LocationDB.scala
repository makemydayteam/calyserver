package com.example.model.db

import com.github.tminglei.slickpg.`[_,_]`
import com.example.model.MyPostgresDriver.api._
import slick.model.ForeignKeyAction
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import com.example.model.Location

trait LocationDB extends UserDB {
	val db : Database

	/*
	 * Locations
	 */
	class Locations(tag: Tag) extends Table[Location](tag, "locations") {
		def * = (id, owner, geoName, longitude, latitude, usersName) <> (Location.tupled, Location.unapply)
		/** Maps whole row to an option. Useful for outer joins. */

		/** Database column id SqlType(int8), PrimaryKey */
		val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
		/** Database column owner SqlType(int8) */
		val owner: Rep[Long] = column[Long]("owner")
		/** Database column geo_name SqlType(varchar) */
		val geoName: Rep[String] = column[String]("geo_name")
		/** Database column users_name SqlType(varchar), Default(None) */
		val usersName: Rep[Option[String]] = column[Option[String]]("users_name", O.Default(None))
		/** Database column longitude SqlType(float8) */
		val longitude: Rep[Double] = column[Double]("longitude")
		/** Database column latitude SqlType(float8) */
		val latitude: Rep[Double] = column[Double]("latitude")

		/** Foreign key referencing Users (database name locations_owner_fkey) */
		lazy val usersFk = foreignKey("locations_owner_fkey", owner, users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
	}

	val locations = TableQuery[Locations]

	def addLocation(location: Location): Future[Option[Location]] = {
		println("add location: " + location)
		val action = ((locations returning locations.map(_.id)) += location)

		db.run(action).map(id => {Some(location.copy(id = id))})
	}

	def getLocation(id: Long): Future[Option[Location]] = {
		var query = locations.filter(l => l.id === id).result.headOption

		db.run(query)
	}

	def getAllUserLocations(userid: Long) : Future[Seq[Location]] = {
		val query = locations.filter(l => l.owner === userid).result

		db.run(query)
	}

	def updateLocation(location: Location): Future[Option[Location]] = {
		var query = locations.filter(l => l.id === location.id && l.owner === location.owner).map(l => (l.geoName, l.usersName, l.longitude, l.latitude))
		var action = query.update((location.geoName, location.usersName, location.longitude, location.latitude))

		var affectedRows : Future[Int] = db.run(action)

		affectedRows.map(n => if (n > 0) Some(location) else None)
	}

	def searchForLocation(userid: Long, searchText: String) : Future[Seq[Location]] = {
		val levenshtein = SimpleFunction.binary[String, String, Long]("levenshtein")
		val levenshtein2 = SimpleFunction.binary[Option[String], String, Long]("levenshtein")

		//case class LocationsSearchRow(id: Long, owner: Long, geoName: String, usersName: Option[String] = None, longitude: Double, latitude: Double, relevance: Long)

		var getByGeoName = locations.filter(l => l.owner === userid).map(l => (l, levenshtein(l.geoName, searchText)))
		var getByUsersName = locations.filter(l => l.owner === userid).map(l => (l, levenshtein2(l.usersName, searchText)))

		var q1 = (getByGeoName ++ getByUsersName).groupBy(_._1)
		var q2 = q1.map { case (l, q) =>
			(l, q.map(_._2).min)
		}

		var query = q2.sortBy(_._2).take(2).result

		db.run(query).map(_ map {_._1})
	}

	/*
	 * Routes
	 */
	case class RoutesRow(startid : Long, endid : Long, duration: Int, distance: Int)

	class Routes(tag: Tag) extends Table[RoutesRow](tag, "routes") {
		def * = (startid, endid, duration, distance) <> (RoutesRow.tupled, RoutesRow.unapply)

		/** Database column startid SqlType(int8) */
		val startid: Rep[Long] = column[Long]("startid")
		/** Database column endid SqlType(int8) */
		val endid: Rep[Long] = column[Long]("endid")
		/** Database column duration SqlType(int4) */
		val duration: Rep[Int] = column[Int]("duration")
		/** Database column distance SqlType(int4) */
		val distance: Rep[Int] = column[Int]("distance")

		/** Primary key of Routes (database name routes_pkey) */
		val pk = primaryKey("routes_pkey", (startid, endid))

		/** Foreign key referencing Locations (database name routes_endid_fkey) */
		lazy val locationsFk1 = foreignKey("routes_endid_fkey", endid, locations)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
		/** Foreign key referencing Locations (database name routes_startid_fkey) */
		lazy val locationsFk2 = foreignKey("routes_startid_fkey", startid, locations)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
	}

	val routes = TableQuery[Routes]

	def getRoute(startid: Long, endid: Long) : Future[Option[RoutesRow]] = {
		/*var query = routes.filter(r => r.startid === startid && r.endid === endid).result.headOption

		db.run(query)*/
		Future(Some(RoutesRow(startid, endid, 0, 0)))
	}

	def addRoutes(newRoutes : Seq[RoutesRow]) : Future[Option[Int]] = {
		val action = (routes ++= newRoutes)

		db.run(action)
	}

}
