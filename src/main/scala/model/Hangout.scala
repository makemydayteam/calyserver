package com.example.model

import org.joda.time.{DateTime, Period, Interval, LocalTime}

case class LocalTimeInterval(start: LocalTime, end: LocalTime)

case class Hangout(
	id: Option[Long] = None,
	owner: Option[Long] = None,
	name: String,
	location: Option[AbstractLocation] = None,
	description: Option[String] = None,
	duration: Period,
	timeFence: Option[Interval] = None,
	dayTime: Option[LocalTimeInterval] = None,
	tags: Seq[String],
	time: Option[Interval],
	attendees: Seq[IdentifiableUser],

	/* only for client usage */
	isOwner: Option[Boolean] = None,
	accepted: Option[Boolean] = None
) {
	def isValid = true
}
