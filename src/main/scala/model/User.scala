package com.example.model

import org.joda.time.LocalTime

trait IdentifiableUser {
	def id : Long
}

abstract class AbstractUser {
	def name: Option[String]
	def firstname: Option[String]

	def email:String
	def password: Option[String]
	
	def address: Option[String]
	
	def timezone : String
	def dayStart : LocalTime
	def dayEnd : LocalTime

	def isValid =  {
		!email.trim.isEmpty &&
		password.map(!_.trim.isEmpty).getOrElse(true)
	}
}

case class NewUser(
	name: Option[String] = None,
	firstname: Option[String] = None,
	email:String,
	password: Option[String] = None,
	address:Option[String] = None,
	timezone: String,
	dayStart: LocalTime,
	dayEnd: LocalTime) extends AbstractUser

case class User(
	id: Long,
	name: Option[String] = None,
	firstname: Option[String] = None,
	email:String,
	password: Option[String] = None,
	address:Option[String] = None,
	timezone: String,
	dayStart: LocalTime,
	dayEnd: LocalTime) extends AbstractUser with IdentifiableUser

object User extends ((Long, Option[String], Option[String], String, Option[String], Option[String], String, LocalTime, LocalTime) => User) {
	def apply(id: Long, content: NewUser) : User = {
		User(id,
			content.name,
			content.firstname,
			content.email,
			content.password,
			content.address,
			content.timezone,
			content.dayStart,
			content.dayEnd)
	}
}

case class  UserRef(id: Long) extends IdentifiableUser
