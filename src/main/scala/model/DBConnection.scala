package com.example.model

import com.example.model.MyPostgresDriver.api._
import java.net.URI
import com.typesafe.config.ConfigFactory

trait DBConnection {

	def getDB() = {
		val conf = ConfigFactory.load();
	    val dbUri = new URI(conf.getString("db.url"))
	    val username = dbUri.getUserInfo.split(":")(0)
	    val password = dbUri.getUserInfo.split(":")(1)
	    val dbUrl = s"jdbc:postgresql://${dbUri.getHost}:${dbUri.getPort}${dbUri.getPath}"
	    Database.forURL(dbUrl, username, password, driver=conf.getString("db.driver"), keepAliveConnection = conf.getBoolean("db.keepAliveConnection"))
	}

	val db = getDB
}
