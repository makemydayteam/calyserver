package com.example.model

case class TagStatisticItem(id: Long, name: String, use: Int, duration: Int)