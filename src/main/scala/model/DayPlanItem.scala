package com.example.model

import org.joda.time.{DateTime, Interval}

case class DayPlanItem(
	appointment: Option[Appointment] = None,
	todo: Option[Todo] = None,
	hangout: Option[Hangout] = None,
	starttime: DateTime,
	endtime: DateTime
)

object AppointmentItem {
	def unapply(item: DayPlanItem) : Option[Appointment] = item.appointment
}

object HangoutItem {
	def unapply(item: DayPlanItem) : Option[Hangout] = item.hangout
}

object FixedTimeItem {
	def unapply(item: DayPlanItem) : Option[Interval] = {
		item match {
			case DayPlanItem(Some(app), None, None, start, end) => Some(new Interval(start, end))
			case DayPlanItem(None, None, Some(hangout), start, end) => Some(new Interval(start, end))
			case _ => None
		}
	}
}

object TodoItem {
	def unapply(item: DayPlanItem) : Option[Todo] = item.todo
}
