package com.example.model

import slick.driver.PostgresDriver
import com.github.tminglei.slickpg._
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat

trait MyPostgresDriver
    extends ExPostgresDriver
    with PgArraySupport
    with PgDateSupportJoda
    with PgSprayJsonSupport
    with PgNetSupport
    with PgLTreeSupport
    with PgRangeSupport
    with PgHStoreSupport
    with PgSearchSupport {

    override val pgjson = "json"

    trait DateTimeRangeImplicits extends RangeImplicits with DateTimeImplicits { 
        private def toDateTime(s: String) : DateTime = DateTime.parse(s, if(s.indexOf(".") > 0 ) jodaTzDateTimeFormatter else jodaTzDateTimeFormatter_NoFraction)
        private def fromDateTime(v: DateTime) : String =   v.toString(jodaTzDateTimeFormatter)

        implicit val simpleDateTimeRangeTypeMapper = new GenericJdbcType[Range[DateTime]]("tstzrange",
            PgRangeSupportUtils.mkRangeFn(toDateTime),
            PgRangeSupportUtils.toStringFn(fromDateTime))
    }

    ///
    override val api = new API with ArrayImplicits
                             with DateTimeImplicits
                             with JsonImplicits
                             with NetImplicits
                             with LTreeImplicits
                             with DateTimeRangeImplicits
                             with HStoreImplicits
                             with SearchImplicits
                             with SearchAssistants {}
}

object MyPostgresDriver extends MyPostgresDriver
