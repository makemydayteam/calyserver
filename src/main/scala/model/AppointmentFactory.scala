package com.example.model

import org.joda.time.{DateTime, Period, Interval}

sealed abstract class AppointmentFactoryMethod {
	def getTimes(interval: Interval) : Seq[Interval]
}

case class AppointmentListFactory(list: Seq[Interval]) extends AppointmentFactoryMethod {
	def getTimes(interval : Interval) = {
		list.filter(_.overlaps(interval))
	}
}

case class AppointmentPeriodFactory(
	id: Option[Long],
	starttime: DateTime,
	endtime: Option[DateTime],
	period: Period,
	duration: Period
) extends AppointmentFactoryMethod {
	def getTimes(interval: Interval) = {
		def dateRange(from: DateTime, to: DateTime, step: Period): Iterator[DateTime] = Iterator.iterate(from)(_.plus(step)).takeWhile(!_.isAfter(to))

		dateRange(starttime, interval.getEnd, period).map { t =>
			new Interval(t, t.plus(duration))
		}.filter(_.overlaps(interval)).toSeq
	}
}

case class AppointmentExceptionalAttributes(
	oldTime: DateTime,
	time: Option[Interval],
	location: Option[AbstractLocation]
)

case class AppointmentFactory(
	id: Option[Long],
	owner: Option[Long],
	name: String,
	tags: Seq[String],
	location: Option[AbstractLocation],
	description: Option[String],
	factory: AppointmentFactoryMethod,
	exceptions: Seq[AppointmentExceptionalAttributes]
) {
	def getInstances(interval: Interval) : Seq[Appointment] = {

		def findExeption(starttime: DateTime) : Option[AppointmentExceptionalAttributes] = {
			exceptions.find(_.oldTime.isEqual(starttime))
		}

		factory.getTimes(interval).map{ t =>
			val exception = findExeption(t.getStart)

			val (time : Interval, loc : Option[AbstractLocation]) = exception match {
				case Some(AppointmentExceptionalAttributes(_,Some(newTime), l)) => (newTime, if(l.isDefined) l else location)
				case _ => (t, location)
			}

			Appointment(id.get, owner.get, name, tags, loc, description, time)
		}
	}
}
