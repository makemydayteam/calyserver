package com.example.model

import org.joda.time.Interval

case class Appointment(
	id: Long,
	owner: Long,
	name: String,
	tags: Seq[String],
	location: Option[AbstractLocation] = None,
	description: Option[String] = None,
	time: Interval
)
