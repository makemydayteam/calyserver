package com.example.model

case class Coordinates(latitude: Double, longitude: Double)
