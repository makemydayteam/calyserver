package com.example.model

sealed abstract class AbstractLocation {
	def id : Long
}

case class Location(
	id: Long,
	owner: Long,
	geoName: String,
	latitude: Double,
	longitude: Double,
	usersName: Option[String]) extends AbstractLocation {
	def isValid() = !geoName.trim.isEmpty && latitude >= -90 && latitude <= 90 && longitude >= -180 && longitude <= 180
}

case class LocationRef(id: Long) extends AbstractLocation

case class JsonLocation(
	id: Option[Long] = None,
	owner: Option[Long] = None,
	geoName: Option[String] = None,
	latitude: Option[Double] = None,
	longitude: Option[Double] = None,
	usersName: Option[String] = None
) {
	def isValid() = !geoName.map(_.trim.isEmpty).getOrElse(false) && latitude.map(lat => lat >= -90 && lat <= 90).getOrElse(false) && longitude.map(long => long >= -180 && long <= 180).getOrElse(false)


	def toLocation : Option[Location] = {
		for {
			i <- id
			o <- owner
			g <- geoName
			lat <- latitude
			long <- longitude
		} yield Location(i, o, g, lat, long, usersName)
	}

	def toLocationRef : Option[LocationRef] = {
		id.map(LocationRef(_))
	}
}

object JsonLocation {
	def apply(l: Location) : JsonLocation = {
		JsonLocation(Some(l.id), Some(l.owner), Some(l.geoName), Some(l.latitude), Some(l.longitude), l.usersName)
	}
}
