package com.example.web

import akka.actor.{Actor, ActorRef}
import spray.routing._
import spray.json._
import spray.http._
import MediaTypes._

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._

import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.util._
import spray.http.HttpHeaders.`Content-Type`
import spray.http.StatusCodes._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

import spray.routing.authentication._

import com.example.model._
import org.joda.time.{DateTime, Interval}

import com.example.algorithm.Controller

import com.example.web.routes._

// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class MyServiceActor() extends Actor with MyService {
  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(myRoute)
}


// this trait defines our service behavior independently from the service actor
trait MyService extends HttpService with ErrorHandler
  with UserRoute
  with TodoRoute
  with AppointmentRoute
  with HangoutRoute
  with LocationRoute
  with DayRoute
  with StatisticRoute
  with AppointmentGroupRoute
{

  implicit val timeout = Timeout(5.seconds)

  val myRoute =
    path("") {
      get {
        respondWithMediaType(`text/html`) { // XML is marshalled to `text/xml` by default, so we simply override here
          complete {
            <html>
              <body>
                <h1>Say hello to <i>spray-routing</i> on <i>spray-can</i>!</h1>
              </body>
            </html>
          }
        }
      }
    } ~
    userRoute ~
    authenticate(BasicAuth(Controller.authenticate _, realm = "secure site")) { user =>
      todoRoute(user) ~
      locationRoute(user) ~
      dayRoute(user)~
      statisticRoute(user) ~
      hangoutRoute(user) ~
      appointmentRoute(user) ~
      appointmentGroupRoute(user)
    }
}
