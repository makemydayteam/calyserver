package com.example.web.routes

import spray.routing.{Route,HttpService, SchemeRejection}
import com.example.model.{User, TagStatisticItem}
import com.example.algorithm.Controller

import spray.json._
import CalyJsonProtocol._

import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.http.StatusCodes
import spray.http.StatusCode

import spray.routing.authentication._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

import org.joda.time.{DateTime, Interval}

trait StatisticRoute extends HttpService {
  this : ErrorHandler =>

  val statisticRoute = (user : User) => path("stats" / "tags" / Segment / Segment) { (start, end) =>
    get {
      try {
        val starttime = org.joda.time.DateTime.parse(start, org.joda.time.format.ISODateTimeFormat.dateTimeParser)
        val endtime = org.joda.time.DateTime.parse(end, org.joda.time.format.ISODateTimeFormat.dateTimeParser)

        val fixtures = Seq[TagStatisticItem] (
          TagStatisticItem(1, "Arbeit", 7, 40),
          TagStatisticItem(2, "Uni", 25, 20),
          TagStatisticItem(3, "Schlaf", 7, 70),
          TagStatisticItem(4, "Fahrzeiten", 2, 5),
          TagStatisticItem(5, "Sport", 4, 10),
          TagStatisticItem(6, "Freizeit", 10, 23)
        )

        complete(StatusWrapper("ok", fixtures))
      } catch {
        case e: Exception => reject(SchemeRejection("Datetime must be formatted according to ISO 8601"))
      }
    }
  }
}
