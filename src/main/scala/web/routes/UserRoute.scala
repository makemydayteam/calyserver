package com.example.web.routes

import spray.routing.HttpService
import com.example.model.{User, NewUser}
import com.example.algorithm.Controller

import spray.json._
import CalyJsonProtocol._

import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.http.StatusCodes

import spray.routing.authentication._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

trait UserRoute extends HttpService {
  val userRoute = pathPrefix("user") {
    post {
      entity(as[NewUser]) { user =>
        validate(user.isValid, " ") {
          onComplete(Controller.addUser(user).map(_.get)) {
            case Success(user: User) => complete(StatusWrapper("ok", user))
            case _ => complete(StatusCodes.NotFound)
          }
        }
      }
    } ~
    authenticate(BasicAuth(Controller.authenticate _, realm = "secure site")) { user =>
      path(Rest) { email =>
        authorize(email == user.email) {
          put {
            entity(as[NewUser]) { user =>
              validate(user.isValid, " ") {
                onComplete(Controller.updateUser(email, user)) {
                  case Success(true) => complete(StatusWrapper("ok", user))
                  case _ => reject
                }
              }
            }
          } ~
          get {
            onComplete(Controller.getUser(email).map(_.get))  {
              case Success(user: User) => complete(StatusWrapper("ok", user))
              case _ => reject
            }
          } ~
          delete {
            onComplete(Controller.deleteUser(email)) {
              case _ => complete((StatusCodes.OK, StatusWrapper("ok", user)))
            }
          }
        }
      } ~
      path("search_by_email" / Rest) { email =>
        get {
          onComplete(Controller.getUser(email).map(_.get)) {
            case Success(user: User) => complete(StatusWrapper("ok", user))
            case _ => reject
          }
        }
      }
    }
  }
}
