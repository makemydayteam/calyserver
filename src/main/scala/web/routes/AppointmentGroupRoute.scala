package com.example.web.routes

import spray.routing.{Route,HttpService}
import com.example.model.{User, AppointmentFactory}
import com.example.algorithm.Controller

import spray.json._
import CalyJsonProtocol._

import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.http.StatusCodes
import spray.http.StatusCode

import spray.routing.authentication._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

import org.joda.time.{DateTime, Interval}

trait AppointmentGroupRoute extends HttpService {
  this: ErrorHandler =>

  val appointmentGroupRoute = (user : User) => pathPrefix("appointment_group") {
    pathPrefix(LongNumber) { id =>
      pathPrefix(LongNumber) { groupId =>
        path("subscribe") {
          post {
            onComplete(Controller.db.addAppointmentGroupToUser(user.id, groupId)) {
              case Success(true) => complete(StatusWrapper("ok", user))
              case _ => complete(StatusCodes.NotFound)
            }
          }
        } ~
        path("unsubscribe") {
          post {
            onComplete(Controller.db.removeAppointmentGroupFromUser(user.id, groupId)) {
              case Success(true) => complete(StatusWrapper("ok", user))
              case _ => complete(StatusCodes.NotFound)
            }
          }
        }
      } ~
      get {
        onComplete(Controller.db.getGroups(user.id, id)) {
          case Success(f) => complete(StatusWrapper("ok", f))
          case _ => complete(StatusCodes.NotFound)
        }
      }
    } ~
    get {
      onComplete(Controller.db.getAllInstitutions) {
        case Success(f) => complete(StatusWrapper("ok", f))
        case _ => complete(StatusCodes.NotFound)
      }
    }
  }
}
