package com.example.web.routes

import spray.routing.{Route,HttpService}
import com.example.model.{JsonLocation, User}
import com.example.algorithm.Controller

import spray.json._
import CalyJsonProtocol._

import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.http.StatusCodes
import spray.http.StatusCode

import spray.routing.authentication._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

import org.joda.time.{DateTime, Interval}

trait LocationRoute extends HttpService {
  this: ErrorHandler =>

  val locationRoute = (user : User) => pathPrefix("location") {
    post {
      entity(as[JsonLocation]) { l =>
        val location = l.copy(owner = Some(user.id))

        validate(location.isValid, " ") {
          onComplete(Controller.addLocation(location).map(_.get)) {
            case Success(l: JsonLocation) => complete(StatusWrapper("ok", l))
            case _ => jsonifyError(StatusCodes.InternalServerError, "")
          }
        }
      }
    } ~
    get {
      onComplete(Controller.getAllUserLocations(user.id)) {
        case Success(l: Seq[JsonLocation]) => complete(StatusWrapper("ok", l))
        case _ => complete(StatusCodes.NotFound)
      }
    } ~
    path(LongNumber) { id =>
      put {
        entity(as[JsonLocation]) { l =>
          val location = l.copy(owner = Some(user.id), id = Some(id))
          validate(location.isValid, " ") {
            onComplete(Controller.updateLocation(location).map(_.get)) {
              case Success(l: JsonLocation) => complete(StatusWrapper("ok", l))
              case _ => reject
            }
          }
        }
      } ~
      get {
        onComplete(Controller.getLocation(id).map(_.get)) {
          case Success(l: JsonLocation) => complete(StatusWrapper("ok", l))
          case _ => complete(StatusCodes.NotFound)
        }
      } ~
      path("search" / Segment) { searchText =>
        get {
          onComplete(Controller.searchForLocation(user.id, searchText)) {
            case Success(l: Seq[JsonLocation]) => complete(StatusWrapper("ok", l))
            case _ => complete(StatusCodes.NotFound)
          }
        }
      }
    }
  }
}
