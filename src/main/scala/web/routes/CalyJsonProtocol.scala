package com.example.web.routes

import com.example.util.BasicJsonProtocol
import com.example.model._

import org.joda.time.{DateTime, LocalTime}
import org.joda.time.format.ISODateTimeFormat

import spray.json._
import DefaultJsonProtocol._

//import com.example.webservices.HereMapsMatrix._

object CalyJsonProtocol extends BasicJsonProtocol {

	/*
	 * Location
	 */
	implicit val locationRefFormat = jsonFormat1(LocationRef)
	implicit val locationFormat = jsonFormat6(JsonLocation.apply)
	implicit val dayPlanLocationFormat = jsonFormat6(Location)

	implicit object AbstractLocationFormat extends JsonFormat[AbstractLocation] {

		def write(l: AbstractLocation) = l match {
			case (r : LocationRef) => JsonLocation(id = Some(r.id)).toJson
			case (ll: Location) => JsonLocation(ll).toJson
		}

		def read(value: JsValue) : AbstractLocation = value.asJsObject.getFields("id") match {
        		case Seq(JsNumber(id)) => LocationRef(id.longValue)
       			case _ => deserializationError("Location id required")
      	}
	}

	/*
	 * User
	 */
	implicit val newUserFormat = jsonFormat8(NewUser)
	implicit val userFormat = jsonFormat9(User.apply)
	implicit val userRefFormat = jsonFormat1(UserRef)

	implicit object AbstractUserFormat extends RootJsonFormat[AbstractUser] {

		def write(u: AbstractUser) = u match {
			case (n: NewUser) => n.toJson
			case (u: User) => u.toJson
		}

		def read(value: JsValue) : AbstractUser = value.asJsObject.getFields("id") match {
        		case Seq(JsNumber(id)) => value.convertTo[User]
       			case _ => value.convertTo[NewUser]
      	}
	}

	implicit object IdentifiableUserFormat extends RootJsonFormat[IdentifiableUser] {
		def write(i: IdentifiableUser) = i match {
			case (r: UserRef) => r.toJson
			case (u: User) => u.toJson
		}

		def read(value: JsValue) : IdentifiableUser = value.asJsObject.getFields("email") match {
			case Seq(JsString(email)) => value.convertTo[User]
			case _ => value.convertTo[UserRef]
		}

	}

	/*
	 * Appointment
	 */
	implicit val appointmentListFactoryFormat = jsonFormat1(AppointmentListFactory)
	implicit val appointmentPeriodFactoryFormat = jsonFormat5(AppointmentPeriodFactory)

	implicit object AppointmentFactoryMethodJsonFormat extends RootJsonFormat[AppointmentFactoryMethod] {
    	def write(f : AppointmentFactoryMethod) = f match {
    		case (l:AppointmentListFactory) => l.toJson
    		case (p:AppointmentPeriodFactory) => p.toJson
    	}

    	def read(value: JsValue) = {
    		value.asJsObject.getFields("list") match {
        		case Seq(JsArray(a)) => value.convertTo[AppointmentListFactory]
       			case _ => value.convertTo[AppointmentPeriodFactory]
      		}
    	}
  	}

  	implicit val appointmentExceptionalAttributesFormat = jsonFormat3(AppointmentExceptionalAttributes)
	implicit val appointmentFactoryFormat = jsonFormat8(AppointmentFactory)

	implicit val appointmentFormat = jsonFormat7(Appointment)

	/*
	 * Todo
	 */
	implicit val clientTodoFormat = jsonFormat8(ClientTodo)
	implicit val todoFormat = jsonFormat11(Todo.apply)

	/*
	 * Hangout
	 */
	implicit val LocalTimeIntervalFormat = jsonFormat2(LocalTimeInterval)
	implicit val hangoutFormat = jsonFormat13(Hangout)

	/*
	 * DayPlan
	 */
	implicit val dayPlanItemFormat = jsonFormat5(DayPlanItem)

	/*
	 * Statistik
	 */
	implicit val tagStatisticItemFormat = jsonFormat4(TagStatisticItem)

	/*
	 * Appointment group
	 */
	implicit val institutionFormat = jsonFormat3(InstitutionRow)
	implicit val appointmentsGroupFormat = jsonFormat4(AppointmentGroup)

	/*
	 * Wrappers
	 */
	implicit def statusWrapperFormat[A :JsonFormat] = jsonFormat2(StatusWrapper.apply[A])
	implicit val rejectionFormat = jsonFormat3(Rejection)
}
