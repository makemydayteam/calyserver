package com.example.web.routes

case class StatusWrapper[T](status: String, data: T)
