package com.example.web.routes

import akka.actor.{Actor, ActorRef}
import spray.routing._
import spray.json._
import CalyJsonProtocol._
import spray.http._
import MediaTypes._

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._

import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.util._
import spray.http.HttpHeaders.`Content-Type`
import spray.http.StatusCodes._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

import spray.routing.authentication._

import com.example.model._
import org.joda.time.{DateTime, Interval}

import com.example.algorithm.Controller

trait ErrorHandler {
  this: HttpService =>

  def jsonifyError(status: StatusCode, msg: String) = {
    respondWithMediaType(`application/json`) {
      complete((status, StatusWrapper("error", Rejection(status.toString, status.intValue, msg))))
    }
  }

  implicit val myRejectionHandler = RejectionHandler {
    case Nil ⇒ jsonifyError(NotFound, "The requested resource could not be found.")

    case MalformedRequestContentRejection(msg, _) :: _ ⇒ jsonifyError(BadRequest, "The request content was malformed:\n" + msg)

    case rejections @ (SchemeRejection(_) :: _) ⇒
      val schemes = rejections.collect { case SchemeRejection(scheme) ⇒ scheme }
      jsonifyError(BadRequest, "Uri scheme not allowed, supported schemes: " + schemes.mkString(", "))
  }
}
