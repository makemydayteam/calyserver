package com.example.web.routes

import spray.routing.{Route,HttpService, SchemeRejection}
import com.example.model.{User}
import com.example.algorithm.Controller

import spray.json._
import CalyJsonProtocol._

import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.http.StatusCodes._
import spray.http.StatusCode

import spray.routing.authentication._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

import org.joda.time.{DateTime, Interval}

trait DayRoute extends HttpService {
  this: ErrorHandler =>

  val dayRoute = (user : User) => path("day" / Segment / Segment) { (start, end) =>
    get {
      println("in day route")
      try {
        val starttime = org.joda.time.DateTime.parse(start, org.joda.time.format.ISODateTimeFormat.dateTimeParser)
        val endtime = org.joda.time.DateTime.parse(end, org.joda.time.format.ISODateTimeFormat.dateTimeParser)

        onComplete(Controller.getDayPlan(user, starttime, endtime)) {
          case Success(plan) => complete(StatusWrapper("ok", plan))
          case x => reject
        }
      } catch {
        case e: Exception => reject(SchemeRejection("Datetime must be formatted according to ISO 8601"))
      }
    }
  }
}
