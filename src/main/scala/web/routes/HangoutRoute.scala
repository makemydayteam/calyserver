package com.example.web.routes

import spray.routing.{Route,HttpService}
import com.example.model.{User, Hangout}
import com.example.algorithm.Controller

import spray.json._
import CalyJsonProtocol._

import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.http.StatusCodes
import spray.http.StatusCode

import spray.routing.authentication._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

import org.joda.time.{DateTime, Interval}

trait HangoutRoute extends HttpService {
  this: ErrorHandler =>

  val hangoutRoute = (user : User) => pathPrefix("hangout") {
    post {
      entity(as[Hangout]) { h =>
        val hangout = h.copy(owner = Some(user.id))

        validate(hangout.isValid, " ") {
          onComplete(Controller.addHangout(user, hangout).map(_.get)) {
            case Success(h: Hangout) => complete(StatusWrapper("ok", h))
            case _ => jsonifyError(StatusCodes.InternalServerError, "")
          }
        }
      }
    } ~
    pathPrefix(LongNumber) { id =>
      path("accept") {
        post {
          onComplete(Controller.acceptHangout(user, id)) {
            case Success(true) => complete(StatusCodes.OK)
            case _ => complete(StatusCodes.NotFound)
          }
        }
      } ~
      path("reject") {
        post {
          onComplete(Controller.rejectHangout(user, id)) {
            case Success(true) => complete(StatusCodes.OK)
            case _ => complete(StatusCodes.NotFound)
          }
        }
      } ~
      path("postpone") {
        post {
          onComplete(Controller.postponeHangout(user, id).map(_.get)) {
            case Success(h: Hangout) => complete(StatusWrapper("ok", h))
            case _ => complete(StatusCodes.NotFound)
          }
        }
      } ~
      get {
        onComplete(Controller.getHangout(user, id).map(_.get)) {
          case Success(h: Hangout) => complete(StatusWrapper("ok", h))
          case _ => complete(StatusCodes.NotFound)
        }
      } ~
      put {
        entity(as[Hangout]) { h =>
          val hangout = h.copy(owner = Some(user.id), id = Some(id))

          validate(hangout.isValid, " ") {
            onComplete(Controller.updateHangout(hangout).map(_.get)) {
              case Success(h: Hangout) => complete(StatusWrapper("ok", h))
              case _ => reject
            }
          }
        }
      } ~
      delete {
        onComplete(Controller.deleteHangout(id)) {
          case _ => complete(StatusCodes.OK)
        }
      }
    }
  }
}
