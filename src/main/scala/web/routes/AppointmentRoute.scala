package com.example.web.routes

import spray.routing.{Route,HttpService}
import com.example.model.{User, AppointmentFactory}
import com.example.algorithm.Controller

import spray.json._
import CalyJsonProtocol._

import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.http.StatusCodes
import spray.http.StatusCode

import spray.routing.authentication._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

import org.joda.time.{DateTime, Interval}

trait AppointmentRoute extends HttpService {
  this: ErrorHandler =>

  val appointmentRoute = (user : User) => pathPrefix("appointment_fac") {
    path(LongNumber) { id =>
      get {
        onComplete(Controller.getAppointmentFactory(id).map(_.get)) {
          case Success(f: AppointmentFactory) => complete(StatusWrapper("ok", f))
          case _ => complete(StatusCodes.NotFound)
        }
      } ~
      put {
        entity(as[AppointmentFactory]) { fac =>
          val f = fac.copy(id = Some(id), owner=Some(user.id))
          onComplete(Controller.updateAppointmentFactory(f).map(_.get)) {
            case Success(f: AppointmentFactory) => complete(StatusWrapper("ok", f))
            case _ => complete(StatusCodes.NotFound)
          }
        }
      } ~
      delete {
        onComplete(Controller.deleteAppointmentFactory(id)) {
          case _ => complete(StatusCodes.OK)
        }
      }
    } ~
    post {
      entity(as[AppointmentFactory]) { f =>
        val fac = f.copy(owner = Some(user.id))

        onComplete(Controller.addAppointmentFactory(fac).map(_.get)) {
          case Success(f: AppointmentFactory) => complete(StatusWrapper("ok", f))
          case _ => jsonifyError(StatusCodes.InternalServerError, "")
        }
      }
    }
  }
}
