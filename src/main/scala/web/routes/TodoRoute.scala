package com.example.web.routes

import spray.routing.{Route,HttpService}
import com.example.model.{User, ClientTodo, NewTodo, Todo, PlannedTodo}
import com.example.algorithm.Controller

import spray.json._
import CalyJsonProtocol._

import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.http.StatusCodes
import spray.http.StatusCode

import spray.routing.authentication._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

import org.joda.time.{DateTime, Interval}

trait TodoRoute extends HttpService {
  this: ErrorHandler =>

  val todoRoute = (user : User) => pathPrefix("todo") {
      post {
        entity(as[ClientTodo]) { t =>
          println("Todo post")
          val todo = NewTodo(user.id, t)

          println(s"todo: ${todo}")

          validate(todo.isValid, " ") {
            println("todo: isValid")
            onComplete(Controller.addTodo(user, todo).map(_.get)) {
              case Success(t: Todo) => complete(StatusWrapper("ok", t))
              case _ => jsonifyError(StatusCodes.InternalServerError, "")
            }
          }
        }
      } ~
      path(LongNumber) { id =>
        put {
          entity(as[ClientTodo]) { t =>
            val todo = Todo(id, PlannedTodo(new Interval(new DateTime, new DateTime().plus(t.duration)), NewTodo(user.id, t)))

            validate(todo.isValid, " ") {
              onComplete(Controller.updateTodo(user, todo).map(_.get)) {
                case Success(todo: Todo) => complete(StatusWrapper("ok", todo))
                case _ => reject
              }
            }
          }
        } ~
        get {
          onComplete(Controller.getTodo(id).map(_.get)) {
            case Success(todo: Todo) => complete(StatusWrapper("ok", todo))
            case _ => reject
          }
        } ~
        delete {
          onComplete(Controller.deleteTodo(id)) {
            case _ => complete(StatusCodes.OK)
          }
        } ~
        path("postpone") {
          put {
            println("in postpone")
            onComplete(Controller.postponeTodo(user, id).map(_.get)) {
              case Success(t: Todo) => complete(StatusWrapper("ok", t))
              case _ => reject
            }
          }
        }
      }
    }
}
