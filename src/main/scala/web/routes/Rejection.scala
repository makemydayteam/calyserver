package com.example.web.routes

case class Rejection(`type`:String, status: Int, message: String)
